#!/usr/bin/env bash

# failures are evil
set -e

# update team statistics
./team-data-update.pl
git add data/team.yaml

# update merge request statistics
./merge-requests-update.py
