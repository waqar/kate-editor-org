<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE language
[
  <!ENTITY identifier "[a-zA-Z_$[:^ascii:]][\w$[:^ascii:]]*">

  <!-- https://tc39.es/ecma262/#sec-literals-numeric-literals -->
  <!ENTITY DecimalIntegerLiteral "(0|[1-9][0-9]*+(_[0-9]++)*+)">
  <!ENTITY DecimalDigits "([0-9]++(_[0-9]++)*+)">
  <!ENTITY ExponentPart "([eE][+-]?&DecimalDigits;)">
  <!ENTITY float "\b&DecimalIntegerLiteral;(\.&DecimalDigits;?&ExponentPart;?|&ExponentPart;)|\.&DecimalDigits;&ExponentPart;?">
]>

<!--
  Author: Anders Lund <anders@alweb.dk>
  Minor changes: Joseph Wenninger <jowenn@kde.org>
  Full JavaScript 1.0 support by Whitehawk Stormchaser

  Last Update: Jun. 03, 2020 (Version 15)

  IMPORTANT: This syntax highlighting definition depends on:
   * JavaScript React (JSX)
   * TypeScript
   * QML
   * CoffeeScript (embedded)
-->


<!-- ***** THIS FILE WAS GENERATED BY A SCRIPT - DO NOT EDIT ***** -->



<language name="JavaScript/Twig" priority="1" alternativeNames="JS" version="26" kateversion="5.79" section="Other" extensions="*.js.twig;*.mjs.twig;*.cjs.twig"
          mimetype="text/x-twig" indenter="cstyle"
          author="Anders Lund (anders@alweb.dk), Joseph Wenninger (jowenn@kde.org), Whitehawk Stormchaser (zerokode@gmx.net)" license="">

  <highlighting>

    <list name="controlflow"><include>controlflow##JavaScript</include></list>
    <list name="keywords"><include>keywords##JavaScript</include></list>
    <list name="reserved"><include>reserved##JavaScript</include></list>
    <list name="module"><include>module##JavaScript</include></list>
    <list name="primitives"><include>primitives##JavaScript</include></list>

    <!-- Built-in Classes, Functions & Constants -->

    <list name="class_builtin"><include>class_builtin##JavaScript</include></list>
    <!-- DOM Classes -->
    <list name="class_dom"><include>class_dom##JavaScript</include></list>
    <!-- Node.js Classes -->
    <list name="class_node"><include>class_node##JavaScript</include></list>

    <!-- General Properties of Classes -->
    <list name="variable_property"><include>variable_property##JavaScript</include></list>
    <!-- General Built-in Functions -->
    <list name="functions"><include>functions##JavaScript</include></list>

    <!-- Functions of "console" -->
    <list name="console_functions"><include>console_functions##JavaScript</include></list>
    <!-- Properties of "process" -->
    <list name="process_property"><include>process_property##JavaScript</include></list>
    <!-- Functions of "process" -->
    <list name="process_functions"><include>process_functions##JavaScript</include></list>

    <!-- Constants of Number Class -->
    <list name="number_constant"><include>number_constant##JavaScript</include></list>
    <!-- Constants of Math Class-->
    <list name="math_constant"><include>math_constant##JavaScript</include></list>
    <!-- Methods of Math Class -->
    <list name="math_functions"><include>math_functions##JavaScript</include></list>
    <!-- Methods of JSON Class -->
    <list name="json_functions"><include>json_functions##JavaScript</include></list>
    <!-- Methods of Promise Class -->
    <list name="promise_functions"><include>promise_functions##JavaScript</include></list>

    <!-- Node.js Variables -->
    <list name="node_variable"><include>node_variable##JavaScript</include></list>
    <!-- DOM Variables -->
    <list name="dom_variable"><include>dom_variable##JavaScript</include></list>
    <!-- DOM Constants -->
    <list name="dom_constant"><include>dom_constant##JavaScript</include></list>

    <!-- For Autocomplete Keywords -->
    <!-- Properties of DOM Classes -->
    <list name="dom_property"><include>dom_property##JavaScript</include></list>
    <!-- Methods of DOM Classes -->
    <list name="dom_functions"><include>dom_functions##JavaScript</include></list>
    <!-- Event Handler -->
    <list name="eventhandler_property"><include>eventhandler_property##JavaScript</include></list>
    <!-- Others Methods of Classes -->
    <list name="function_common"><include>function_common##JavaScript</include></list>

    <contexts>

      <!-- Shebang: ("#!") Detect shebang and fallthrough to Normal -->
      <!-- Based on the similar clause from prolog.xml.
           This uses "lineEmptyContext" & "lineEndContext" to force the highlight only on the first line. -->
      <context name="Shebang" attribute="Normal Text" lineEndContext="Normal" lineEmptyContext="Normal" fallthroughContext="Normal">
<IncludeRules context="##Twig/Twig" />
        <Detect2Chars column="0" char="#" char1="!" context="Shebang Line" attribute="Comment" />
      </context>
      <context name="Shebang Line" attribute="Comment" lineEndContext="#pop!Normal" >
<IncludeRules context="##Twig/Twig" />
</context>

      <!-- Normal Syntax of JavaScript. -->
      <!-- NOTE: These IncludeRules rules, except BuiltInClasses, FindBrackets, FindPrivateOperator, FindE4X are included in qml.xml -->
      <context attribute="Normal Text" lineEndContext="#stay" name="Normal">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces/>
        <IncludeRules context="FindSomeSymbols" />
        <IncludeRules context="FindBrackets" />
        <IncludeRules context="FindPrivateOperator" />
        <IncludeRules context="FindComments" />
        <IncludeRules context="FindNumbers" />
        <IncludeRules context="FindMembers" />
        <IncludeRules context="FindKeywords" />
        <IncludeRules context="FindStrings" />
        <IncludeRules context="BuiltInClasses" />
        <IncludeRules context="FindObjectMembersAndFunctions" />
        <IncludeRules context="FindIdentifier" />
        <IncludeRules context="FindE4X" />
        <IncludeRules context="FindRegex" />
      </context>
      <context attribute="Normal Text" lineEndContext="#stay" name="FindSomeSymbols">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="FindArrowFunction" />
        <!-- symbol that is not used in any other context (fast path) -->
        <AnyChar attribute="Symbol" String=":!%&amp;+,-*=&gt;?|~^;" />
      </context>

      <!-- Base Syntax. Used by the TypeScript highlighter. -->
      <context attribute="Normal Text" lineEndContext="#stay" name="JSBase">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Symbol" char="?" />
        <IncludeRules context="FindIdentifier" />
        <IncludeRules context="FindE4X" />
        <IncludeRules context="FindComments" />
        <IncludeRules context="FindRegex" />
      </context>
      <context attribute="Normal Text" lineEndContext="#stay" name="JSBaseSomeSymboles">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="FindArrowFunction" />
        <!-- symbol that is not used in any other context (fast path) -->
        <AnyChar attribute="Symbol" String="!%&amp;+,-*=&gt;|~^;" />
      </context>


      <!-- Content Contexts -->

      <!-- Avoid highlighting regular expressions. -->
      <context attribute="Normal Text" lineEndContext="#stay" fallthroughContext="#pop" name="NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <Detect2Chars context="#pop" char="/" char1="/" lookAhead="true" />
        <Detect2Chars context="#pop" char="/" char1="*" lookAhead="true" />
        <DetectChar attribute="Symbol" context="#pop" char="/" />
        <DetectSpaces/>
      </context>

      <!-- USE ONLY ANFTER STRINGS: Highlight only valid regular expressions of a single line. -->
      <context attribute="Normal Text" lineEndContext="#stay" fallthroughContext="#pop#pop" name="RegExpAfterString">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces/>
        <Detect2Chars context="#pop#pop" char="/" char1="/" lookAhead="true" />
        <Detect2Chars context="#pop#pop" char="/" char1="*" lookAhead="true" />
        <!-- Detect valid Regular expression in single line -->
        <RegExpr attribute="Regular Expression" context="#pop#pop!(regex caret first check)" String="/(?=(?:[^/\\\[]|\\.|\[(?:[^\]\\]|\\.)+\])+/(?:[gimsuy]+(?!\s*[\w$])|(?![/\*])|(?=/\*)))" />
        <DetectChar attribute="Symbol" context="#pop#pop" char="/" />
      </context>

      <!-- E4X extension (=xml inside JavaScript), see bug #373713 -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindE4X">
<IncludeRules context="##Twig/Twig" />
        <RegExpr attribute="Symbol" context="#stay" String="&lt;(?=[^/]|/\*|//|$)" />
        <Detect2Chars attribute="Symbol" context="NoRegExp" char="&lt;" char1="/" />
        <Detect2Chars attribute="Symbol" context="NoRegExp" char="/" char1="&gt;" />
      </context>

      <!--DetectIdentifier-->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindIdentifier">
<IncludeRules context="##Twig/Twig" />
        <!-- Do not start RegExp after non-ASCII characters (see bug #393633) -->
        <RegExpr context="NoRegExp" String="&identifier;" />
      </context>

      <context attribute="Normal Text" lineEndContext="#stay" name="FindRegex">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Regular Expression" context="(regex caret first check)" char="/" />
      </context>

      <!-- Object, within { ... } -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindBrackets">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Normal Text" context="#stay" char="(" />
        <DetectChar attribute="Normal Text" context="NoRegExp" char=")" />
        <DetectChar attribute="Normal Text" context="Object" char="{" beginRegion="Brace" />
        <DetectChar attribute="Normal Text" context="#stay" char="[" beginRegion="List" />
        <DetectChar attribute="Normal Text" context="NoRegExp" char="]" endRegion="List" />
      </context>

      <context attribute="Normal Text" lineEndContext="#stay" name="Object">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Normal Text" context="#pop" char="}" endRegion="Brace" />
        <!-- Mostly required for define: in switch blocks -->
        <keyword attribute="Keyword" String="keywords" />
        <RegExpr attribute="ControlFlow" String="\bdefault(?=\s*:)" />

        <RegExpr attribute="JSON" String="&identifier;(?=\s*:)" />
        <!-- a ? b : c -->
        <RegExpr attribute="Symbol" context="ConditionalExpression" String="\?(?=[^\{\}]*&identifier;\s*\:)" />

        <IncludeRules context="Normal" />
      </context>
      <!-- Highlight keywords before ":" in conditional expressions. -->
      <context attribute="Normal Text" lineEndContext="#pop" name="ConditionalExpression">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Symbol" context="#pop" char=":" />
        <DetectChar attribute="Symbol" context="#stay" char="?" />
        <AnyChar context="#pop" String="{}" lookAhead="true" />
        <IncludeRules context="FindKeywords" />
        <IncludeRules context="Normal" />
      </context>

      <!-- '#' in "#identifier" -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindPrivateOperator">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Private Member Operator" char="#" />
      </context>

      <!-- Keywords -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindKeywords">
<IncludeRules context="##Twig/Twig" />
        <WordDetect attribute="Keyword" context="FunctionDeclaration" String="function" />
        <keyword attribute="ControlFlow" String="controlflow" />
        <keyword attribute="Keyword" String="keywords" />
        <keyword attribute="Reserved" String="reserved" />
        <keyword attribute="Keyword" context="NoRegExp" String="primitives" />
        <keyword attribute="Module" String="module" />
      </context>
      <context attribute="Normal Text" lineEndContext="#stay" name="FunctionDeclaration" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <IncludeRules context="FindPrivateOperator" />
        <RegExpr attribute="Function Declaration" context="#pop" String="&identifier;(?=\s*\()" /> <!-- NoRegExp -->
        <IncludeRules context="FindComments" />
      </context>

      <!-- Numbers -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindNumbers">
<IncludeRules context="##Twig/Twig" />
        <RegExpr attribute="Float" context="NoRegExp" String="&float;" />
        <AnyChar context="Integer" String="0123456789" lookAhead="1"/>
      </context>
      <context attribute="Normal Text" lineEndContext="#stay" name="Integer">
<IncludeRules context="##Twig/Twig" />
        <RegExpr attribute="Hex" context="#pop!NumericSufix" String="\b0[xX][0-9a-fA-F]++(_[0-9a-fA-F]++)*+"/>
        <!-- 07 is octal, 08 is decimal -->
        <RegExpr attribute="Octal" context="#pop!NumericSufix" String="\b0([oO][0-7]++(_[0-7]++)*+|0*+[1-7][0-7]*+(_[0-7]++)*+(?!_?[89]))" />
        <RegExpr attribute="Binary" context="#pop!NumericSufix" String="\b0[bB][01]++(_[01]++)*+" />
        <!-- 0000_8 is invalid -->
        <RegExpr attribute="Decimal" context="#pop!NumericSufix" String="\b0*+([1-9][0-9]*+(_[0-9]++)*+)?"/>
      </context>
      <context attribute="Normal Text" lineEndContext="#pop" name="NumericSufix" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Normal Text" context="#pop!NoRegExp" char="n" />
        <IncludeRules context="NumericSufixError" />
      </context>
      <context attribute="Normal Text" lineEndContext="#pop" name="NumericSufixError">
<IncludeRules context="##Twig/Twig" />
        <RegExpr attribute="Error" context="#pop!NoRegExp" String="[0-9]*(&identifier;)?"/>
      </context>

      <!-- Find => -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindArrowFunction">
<IncludeRules context="##Twig/Twig" />
        <Detect2Chars attribute="Special Operators" context="#stay" char="=" char1="&gt;" />
      </context>

      <!-- Find Objects Member -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindMembers">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Symbol" context="Object Member" char="." lookAhead="true" />
      </context>

      <!-- Find Objects Member and Functions -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindObjectMembersAndFunctions">
<IncludeRules context="##Twig/Twig" />
        <!-- Tagged Template Literals -->
        <RegExpr attribute="Function Name" context="#stay" String="&identifier;(?=\s*`)" />
        <!-- The order of these rules is important: 1) Functions. 2) Constants. 3) Objects. -->
        <RegExpr attribute="Function Name" context="Function" String="(&identifier;)(?=\s*\()" lookAhead="true" />
        <IncludeRules context="BuiltInConstants" />
        <RegExpr attribute="Objects" context="Object Member" String="&identifier;(?=\s*\.)" />
      </context>
      <context attribute="Normal Text" lineEndContext="#pop" name="Function">
<IncludeRules context="##Twig/Twig" />
        <keyword attribute="Function (Built-in)" context="#pop" String="functions" />
        <StringDetect attribute="Function Name" context="#pop" String="%1" dynamic="true" />
        <DetectChar context="#pop" char="(" lookAhead="true" />
      </context>

      <context attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop" name="Object Member">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="FindPrivateOperator" />
        <IncludeRules context="DefaultMemberObject" />
        <!-- Common functions and properties, only for autocomplete keywords -->
        <keyword attribute="Object Member" context="#pop!NoRegExp" String="dom_property" />
        <keyword attribute="Function Name" context="#pop!NoRegExp" String="dom_functions" />
        <keyword attribute="Object Member" context="#pop!NoRegExp" String="eventhandler_property" />
        <keyword attribute="Function Name" context="#pop!NoRegExp" String="function_common" />
      </context>
      <!-- Used in common and built-in objects. -->
      <context attribute="Normal Text" lineEndContext="#pop" name="DefaultMemberObject">
<IncludeRules context="##Twig/Twig" />
        <StringDetect attribute="Symbol" context="#pop" String="..." />
        <DetectChar attribute="Symbol" context="#stay" char="." />

        <!-- The order of these rules is important: 1) Functions. 2) Constants & Properties. 3) Obj. Members. -->
        <!-- Function -->
        <RegExpr attribute="Function Name" context="#pop" String="&identifier;(?=\s*\()" />
        <!-- Tagged Template Literals -->
        <RegExpr context="#pop" String="&identifier;\s*`" lookAhead="true" />
        <!-- Generic constants and properties -->
        <keyword attribute="Object Property (Built-in)" context="#pop!NoRegExp" String="variable_property" />
        <keyword attribute="Constant" context="#pop!NoRegExp" String="dom_constant" />

        <RegExpr attribute="Object Member" context="#stay" String="&identifier;(?=\s*\.)" />
        <RegExpr attribute="Object Member" context="#pop!NoRegExp" String="&identifier;" />
        <IncludeRules context="NoRegExp" />

        <!-- Generic Functions -->
        <keyword attribute="Function Name" context="#pop!NoRegExp" String="functions" />
      </context>

      <!-- Strings -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindStrings">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Template" context="Template" char="`" beginRegion="Template" />
        <RegExpr attribute="Template" context="StartRawTemplate" String="\b(String)\s*(\.)\s*(raw)\s*`" lookAhead="true" />

        <DetectChar attribute="String" context="String" char="&quot;" />
        <DetectChar attribute="String" context="String SQ" char="'" />
      </context>

      <context attribute="String" lineEndContext="#pop" name="String">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="Escape" />
        <DetectChar attribute="String" context="RegExpAfterString" char="&quot;" />
      </context>
      <context attribute="String" lineEndContext="#pop" name="String SQ">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="Escape" />
        <DetectChar attribute="String" context="RegExpAfterString" char="'" />
      </context>

      <context attribute="Normal Text" lineEndContext="#stay" name="Escape">
<IncludeRules context="##Twig/Twig" />
        <LineContinue attribute="Escape" context="#stay" />
        <!-- Hexadecimal, unicode & octal -->
        <RegExpr attribute="Escape" context="#stay" String="\\(?:x[0-9a-fA-F]{2}|u[0-9a-fA-F]{4}|u\{[0-9a-fA-F]+\}|[0-3][0-7]{1,2}|[4-7][0-7]|[1-7])" />
        <Detect2Chars attribute="Error" context="#stay" char="\" char1="x" />
        <Detect2Chars attribute="Error" context="#stay" char="\" char1="u" />
        <DetectChar attribute="Escape" context="Character Escape" char="\" />
      </context>
      <context attribute="Escape" lineEndContext="#pop" name="Character Escape">
<IncludeRules context="##Twig/Twig" />
        <AnyChar attribute="Reserved Char. Escape" context="#pop" String="0bfnrtv" /> <!-- Reserved Char. Escape. Also: "'/ -->
        <RegExpr attribute="Escape" context="#pop" String="." /> <!-- Literal Char. Escape -->
      </context>

      <!-- Template -->
      <context attribute="Template" lineEndContext="#stay" name="Template">
<IncludeRules context="##Twig/Twig" />
        <IncludeRules context="Escape" />
        <Detect2Chars attribute="Substitution" context="Substitution" char="$" char1="{" />
        <DetectChar attribute="Template" context="RegExpAfterString" char="`" endRegion="Template" />
      </context>
      <context attribute="Normal Text" lineEndContext="#stay" name="Substitution">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Substitution" char="}" context="#pop" />
        <IncludeRules context="Normal" />
      </context>

      <context attribute="Normal Text" lineEndContext="#pop" name="StartRawTemplate">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar attribute="Template" context="#pop!RawTemplate" char="`" beginRegion="Template" />
        <StringDetect attribute="Built-in Objects" context="#stay" String="%1" dynamic="true" />
        <DetectChar attribute="Symbol" context="#stay" char="2" dynamic="true" />
        <StringDetect attribute="Function Name" context="#stay" String="%3" dynamic="true" />
      </context>
      <context attribute="Template" lineEndContext="#stay" name="RawTemplate">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Template" context="RegExpAfterString" char="`" endRegion="Template" />
      </context>

      <!-- Comments -->
      <context attribute="Normal Text" lineEndContext="#stay" name="FindComments">
<IncludeRules context="##Twig/Twig" />
        <Detect2Chars attribute="Comment" context="MatchComment" char="/" char1="/" lookAhead="true"/>
        <Detect2Chars attribute="Comment" context="MatchComment" char="/" char1="*" lookAhead="true" />
      </context>
      <context name="MatchComment" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <StringDetect attribute="Region Marker" context="#pop!region_marker" String="//BEGIN" beginRegion="Region1" />
        <StringDetect attribute="Region Marker" context="#pop!region_marker" String="//END" endRegion="Region1" />

        <IncludeRules context="##Doxygen" />
        <Detect2Chars attribute="Comment" context="#pop!Comment" char="/" char1="/" />
        <Detect2Chars attribute="Comment" context="#pop!Multi/inline Comment" char="/" char1="*" beginRegion="Comment" />
      </context>
      <context attribute="Comment" lineEndContext="#pop" name="Comment">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <IncludeRules context="##Comments" />
      </context>
      <context attribute="Comment" lineEndContext="#stay" name="Multi/inline Comment">
<IncludeRules context="##Twig/Twig" />
        <Detect2Chars attribute="Comment" context="#pop" char="*" char1="/" endRegion="Comment" />
        <DetectSpaces />
        <IncludeRules context="##Comments" />
      </context>
      <context attribute="Region Marker" lineEndContext="#pop" name="region_marker" >
<IncludeRules context="##Twig/Twig" />
</context>

      <!-- Regular Expressions -->
      <context attribute="Regular Expression" lineEndContext="#stay" name="Regular Expression">
<IncludeRules context="##Twig/Twig" />
        <RegExpr attribute="Regular Expression" context="#pop#pop" String="/\w*" />
        <RegExpr attribute="Pattern Character Class" String="\\[nrtvfDdSsWw]" />
        <DetectChar attribute="Pattern Character Class" context="(charclass caret first check)" char="[" />
        <RegExpr attribute="Pattern Internal Operator" String="\{[\d, ]+\}|\\[bB]|\\.|\$(?=/)|[?+*()|]" />
      </context>
      <context attribute="Pattern Character Class" lineEndContext="#stay" name="Regular Expression Character Class">
<IncludeRules context="##Twig/Twig" />
        <RegExpr String="\\[\[\]]" />
        <RegExpr attribute="Pattern Internal Operator" String="\\." />
        <DetectChar attribute="Pattern Character Class" context="#pop#pop" char="]" />
      </context>
      <context attribute="Pattern Internal Operator" lineEndContext="#pop" name="(regex caret first check)" fallthroughContext="Regular Expression">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Pattern Internal Operator" context="Regular Expression" char="^" />
      </context>
      <context attribute="Pattern Internal Operator" lineEndContext="#pop" name="(charclass caret first check)" fallthroughContext="Regular Expression Character Class">
<IncludeRules context="##Twig/Twig" />
        <DetectChar attribute="Pattern Internal Operator" context="Regular Expression Character Class" char="^" />
      </context>


      <!-- Built-in Classes -->
      <!-- Keep in sync with typescript.xml -->

      <context name="BuiltInClasses" attribute="Normal Text" lineEndContext="#stay">
<IncludeRules context="##Twig/Twig" />
        <WordDetect context="FindMemberObjectMath" attribute="Built-in Objects" String="Math" />
        <WordDetect context="FindMemberObjectNumber" attribute="Built-in Objects" String="Number" />
        <WordDetect context="FindMemberObjectPromise" attribute="Built-in Objects" String="Promise" />
        <WordDetect context="FindMemberObjectJSON" attribute="Built-in Objects" String="JSON" />
        <WordDetect context="FindMemberObjectConsole" attribute="Built-in Objects" String="console" />
        <WordDetect context="FindMemberObjectProcess" attribute="Built-in Objects" String="process" />

        <keyword context="NoRegExp" attribute="Built-in Objects" String="class_builtin" />
        <keyword context="NoRegExp" attribute="Built-in Objects" String="class_dom" />
        <keyword context="FindMemberObjectDOM" attribute="Built-in Objects" String="dom_variable" />
        <keyword context="NoRegExp" attribute="Built-in Objects" String="class_node" />
        <keyword context="NoRegExp" attribute="Built-in Objects" String="node_variable" />
      </context>
      <!-- These constants are highlighted in "Normal" -->
      <context name="BuiltInConstants" attribute="Normal Text" lineEndContext="#stay">
<IncludeRules context="##Twig/Twig" />
        <keyword context="NoRegExp" attribute="Constant" String="math_constant" />
        <keyword context="NoRegExp" attribute="Constant" String="number_constant" />
        <keyword context="NoRegExp" attribute="Constant" String="dom_constant" />
      </context>

      <!-- DOM Variable -->
      <context name="FindMemberObjectDOM" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectDOM" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectDOM" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!FindMemberObjectDOM" attribute="Object Property (Built-in)" String="dom_property" />
        <keyword context="#pop!FindMemberObjectDOM" attribute="Object Method (Built-in)" String="dom_functions" />
        <IncludeRules context="DefaultMemberObject" />
        <!-- Only for autocomplete -->
        <keyword context="#pop!NoRegExp" attribute="Object Member" String="eventhandler_property" />
        <keyword context="#pop!NoRegExp" attribute="Function Name" String="function_common" />
      </context>
      <!-- Math Class -->
      <context name="FindMemberObjectMath" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectMath" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectMath" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Constant" String="math_constant" />
        <keyword context="#pop!NoRegExp" attribute="Object Method (Built-in)" String="math_functions" />
        <IncludeRules context="DefaultMemberObject" />
      </context>
      <!-- Number Class -->
      <context name="FindMemberObjectNumber" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectNumber" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectNumber" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Constant" String="number_constant" />
        <IncludeRules context="DefaultMemberObject" />
      </context>
      <!-- Promise Class -->
      <context name="FindMemberObjectPromise" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectPromise" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectPromise" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Object Method (Built-in)" String="promise_functions" />
        <IncludeRules context="DefaultMemberObject" />
      </context>
      <!-- JSON Class -->
      <context name="FindMemberObjectJSON" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectJSON" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectJSON" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Object Method (Built-in)" String="json_functions" />
        <IncludeRules context="DefaultMemberObject" />
      </context>
      <!-- Console Object -->
      <context name="FindMemberObjectConsole" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectConsole" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectConsole" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Object Method (Built-in)" String="console_functions" />
        <IncludeRules context="DefaultMemberObject" />
      </context>
      <!-- Process Object -->
      <context name="FindMemberObjectProcess" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop!NoRegExp">
<IncludeRules context="##Twig/Twig" />
        <DetectSpaces />
        <DetectChar context="#pop!MemberObjectProcess" attribute="Symbol" char="." />
      </context>
      <context name="MemberObjectProcess" attribute="Normal Text" lineEndContext="#pop" fallthroughContext="#pop">
<IncludeRules context="##Twig/Twig" />
        <keyword context="#pop!NoRegExp" attribute="Object Method (Built-in)" String="process_functions" />
        <keyword context="#pop!NoRegExp" attribute="Object Property (Built-in)" String="process_property" />
        <IncludeRules context="DefaultMemberObject" />
      </context>

    </contexts>

    <itemDatas>
      <itemData name="Normal Text"  defStyleNum="dsNormal"  spellChecking="false" />
      <itemData name="Keyword"      defStyleNum="dsKeyword" spellChecking="false" />
      <itemData name="ControlFlow"  defStyleNum="dsControlFlow" spellChecking="false" />
      <itemData name="Reserved"     defStyleNum="dsKeyword" italic="true" spellChecking="false" />
      <itemData name="Module"       defStyleNum="dsImport" spellChecking="false" />
      <itemData name="Objects"      defStyleNum="dsNormal" spellChecking="false" />
      <itemData name="Object Member" defStyleNum="dsAttribute" spellChecking="false" />

      <itemData name="Function Name"    defStyleNum="dsFunction" spellChecking="false" />
      <itemData name="Function Declaration" defStyleNum="dsFunction" spellChecking="false" />
      <itemData name="Built-in Objects" defStyleNum="dsBuiltIn" spellChecking="false" />
      <itemData name="Object Property (Built-in)" defStyleNum="dsAttribute" italic="1" spellChecking="false" />
      <itemData name="Object Method (Built-in)" defStyleNum="dsFunction" italic="1" spellChecking="false" />
      <itemData name="Function (Built-in)" defStyleNum="dsPreprocessor" spellChecking="false" />
      <itemData name="Constant"         defStyleNum="dsConstant" spellChecking="false" />

      <itemData name="Decimal"      defStyleNum="dsDecVal" spellChecking="false" />
      <itemData name="Octal"        defStyleNum="dsBaseN" spellChecking="false" />
      <itemData name="Hex"          defStyleNum="dsBaseN" spellChecking="false" />
      <itemData name="Binary"       defStyleNum="dsBaseN" spellChecking="false" />
      <itemData name="Float"        defStyleNum="dsFloat" spellChecking="false" />
      <itemData name="Escape"       defStyleNum="dsSpecialChar" spellChecking="false" />
      <itemData name="Reserved Char. Escape" defStyleNum="dsSpecialChar" bold="1" spellChecking="false" />
      <itemData name="String"       defStyleNum="dsString" />
      <itemData name="Template"     defStyleNum="dsVerbatimString" />
      <itemData name="Substitution" defStyleNum="dsSpecialChar" spellChecking="false" />

      <itemData name="Comment"      defStyleNum="dsComment" />
      <itemData name="Symbol"       defStyleNum="dsOperator" spellChecking="false" />
      <itemData name="Special Operators" defStyleNum="dsKeyword" spellChecking="false" />
      <itemData name="Private Member Operator" defStyleNum="dsNormal" spellChecking="false" />
      <itemData name="Regular Expression" defStyleNum="dsSpecialString" spellChecking="false" />
      <itemData name="Pattern Internal Operator" defStyleNum="dsSpecialChar" spellChecking="false" />
      <itemData name="Pattern Character Class" defStyleNum="dsSpecialChar" spellChecking="false" />
      <itemData name="Region Marker" defStyleNum="dsRegionMarker" spellChecking="false" />
      <itemData name="JSON"         defStyleNum="dsDataType" bold="0" spellChecking="false" />
      <itemData name="Error"        defStyleNum="dsError" spellChecking="false"/>
    </itemDatas>

  </highlighting>

  <general>
    <comments>
      <comment name="singleLine" start="//" position="afterwhitespace"/>
      <comment name="multiLine" start="/*" end="*/" region="Comment" />
    </comments>
    <keywords casesensitive="1" />
  </general>

</language>

<!-- kate: space-indent on; indent-width 2; -->


<!-- ***** THIS FILE WAS GENERATED BY A SCRIPT - DO NOT EDIT ***** -->
