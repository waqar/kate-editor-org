# SPDX-FileCopyrightText: 2023 Phu Hung Nguyen <phu@kde.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import logging
import os

import polib
from hugo_gettext import config, utils
from hugoi18n.fetch import fetch_langs, revert_lang_code


level = logging.INFO
logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

os.environ["PACKAGE"] = 'websites-kate-editor-org'
# os.system('git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n')
os.system('hugoi18n compile po')  # compile translations in folder "po"
os.system('hugoi18n generate')

# Now fetch svn/l10n-kf6/syntax-highlighting/syntaxhighlighting6_qt.po for languages configured in the config file
config_path = config._find_config_file()
hugo_config = utils.read_file(config_path)
hugo_langs = set(hugo_config['languages'].keys())
po_dir = 'pos'
if os.path.isdir(po_dir):
    logging.info(f'{po_dir} folder already exists, skipping fetch')
else:
    os.environ['PACKAGE'] = 'syntax-highlighting'
    os.environ['FILENAME'] = 'syntaxhighlighting6_qt'
    fetch_langs(hugo_langs, po_dir, as_static=False)

# For each lang in hugo_langs, generate a data file containing translations gotten from the PO file
for hugo_lang_code in hugo_langs:
    lang_code = revert_lang_code(hugo_lang_code)
    po_path = os.path.join(po_dir, f'{lang_code}.po')
    if os.path.isfile(po_path):
        po_file = polib.pofile(po_path)
        translations = {}
        for e in po_file.translated_entries():
            translations[e.msgid] = e.msgstr
        translations_path = f'data/{hugo_lang_code}/syntaxes_themes.yaml'
        os.makedirs(os.path.dirname(translations_path), exist_ok=True)
        utils.write_file(translations_path, translations)
