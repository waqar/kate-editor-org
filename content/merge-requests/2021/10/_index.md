---
title: Merge Requests - October 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/10/
---

#### Week 43

- **Kate - [fix selected option when filtering in quick open](https://invent.kde.org/utilities/kate/-/merge_requests/509)**<br />
Request authored by [Gustavo  Rodrigues](https://invent.kde.org/gustavonr) and merged after 3 days.

- **KTextEditor - [TextFolding::importFoldingRanges: get rid of UB](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/211)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KTextEditor - [Set metadata when creating katepart](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/212)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KSyntaxHighlighting - [nix: Add &#x27; as valid identifier part](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/270)**<br />
Request authored by [Marco Rebhan](https://invent.kde.org/dblsaiko) and merged after 2 days.

- **Kate - [Allow Ctrl-Click to be more flexible](https://invent.kde.org/utilities/kate/-/merge_requests/508)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [wayland fix window activation](https://invent.kde.org/utilities/kate/-/merge_requests/507)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged after one day.

- **Kate - [Remove code to keep lsptooltips on screen on wayland](https://invent.kde.org/utilities/kate/-/merge_requests/506)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged at creation day.

- **Kate - [Show LspClient after positioning](https://invent.kde.org/utilities/kate/-/merge_requests/505)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged at creation day.

- **Kate - [Fix clazy warnings](https://invent.kde.org/utilities/kate/-/merge_requests/500)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 15 days.

#### Week 42

- **Kate - [fixes reload dialog on wayland](https://invent.kde.org/utilities/kate/-/merge_requests/504)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [gitblame: Show commit details in a file treeview](https://invent.kde.org/utilities/kate/-/merge_requests/501)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 13 days.

- **Kate - [Add missing optional deps to CI metadata](https://invent.kde.org/utilities/kate/-/merge_requests/502)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KSyntaxHighlighting - [Fix the colors of modified and saved lines](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/269)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **KTextEditor - [Simplify DocumentCursor::setPosition](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/207)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Highlight Folding Markers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/194)**<br />
Request authored by [Wagner M Cunha](https://invent.kde.org/frosutokun) and merged after 28 days.

- **KTextEditor - [#37 Improve the context menu for selections](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/192)**<br />
Request authored by [Luis Taira](https://invent.kde.org/lhtaira) and merged after 43 days.

- **KSyntaxHighlighting - [cmake.xml: Recognize CMake provided modules and functions/macros](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/267)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 3 days.

- **KSyntaxHighlighting - [Add syntax highlighting for Nix files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/265)**<br />
Request authored by [Marco Rebhan](https://invent.kde.org/dblsaiko) and merged after 2 days.

#### Week 41

- **Kate - [Replace rls with rust-analyzer for rust language server](https://invent.kde.org/utilities/kate/-/merge_requests/495)**<br />
Request authored by [Jonathan Lopez](https://invent.kde.org/otherwisejlo) and merged after 16 days.

- **KSyntaxHighlighting - [Specify byproducts for ExternalProject_Add call](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/264)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KSyntaxHighlighting - [dockerfile.xml: Use `bash` syntax for shell form of commands](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/263)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [Make the regex search fast](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/206)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Port QStringRef (deprecated) to QStringView](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/205)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 4 days.

- **KSyntaxHighlighting - [cmake.xml: Improvements to highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/262)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: Improvements to highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/260)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [spdx-comments.xml.tpl: Backport changes from the generated XML](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/261)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [Improve Appearance &gt; Borders Dialog](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/202)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 7 days.

- **KTextEditor - [KateRenderer: Check for m_view being null in more places](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/204)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after 2 days.

- **KSyntaxHighlighting - [Add support of YAML in Fenced Code Blocks in Markdown](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/258)**<br />
Request authored by [Ksofix Alert](https://invent.kde.org/altk) and merged after one day.

#### Week 40

- **Kate - [Add a frontend for compiler-explorer](https://invent.kde.org/utilities/kate/-/merge_requests/490)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 12 days.

- **Kate - [Snippets: manage a member pointer with a unique_ptr](https://invent.kde.org/utilities/kate/-/merge_requests/497)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [ExternalTools: config file per tool](https://invent.kde.org/utilities/kate/-/merge_requests/484)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 32 days.

- **Kate - [Enable proper CI for Linux and FreeBSD](https://invent.kde.org/utilities/kate/-/merge_requests/498)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [Do not cancel mouse selection when using the keyboard, bis](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/199)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged after one day.

- **KTextEditor - [[KateIconBorder] Handle annotations context menu](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/200)**<br />
Request authored by [Ismael Asensio](https://invent.kde.org/iasensio) and merged after one day.

- **KTextEditor - [Enable enclose selection by default for brackets](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/196)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 5 days.

- **KTextEditor - [Dont indent the lines if line already has text + noindent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/184)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 47 days.

- **Kate - [KTextEditorPreview::KPartView: optimize shortcut code](https://invent.kde.org/utilities/kate/-/merge_requests/496)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KTextEditor - [Improve Open/Save Advanced Dialog](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/197)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 3 days.

#### Week 39

- **Kate - [Fix crash on switching branches with filter text](https://invent.kde.org/utilities/kate/-/merge_requests/491)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KTextEditor - [Better General dialogue for text editing settings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/195)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 4 days.

- **KTextEditor - [[KateIconBorder] Add context menu](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/198)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after one day.

- **KSyntaxHighlighting - [loading rules in 2 parts to reduce the final memory used](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/252)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 32 days.

- **Kate - [The minimum required Qt is 5.14](https://invent.kde.org/utilities/kate/-/merge_requests/493)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 4 days.

- **Kate - [Ship OOB configuration for Elm language server](https://invent.kde.org/utilities/kate/-/merge_requests/494)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 3 days.

