---
title: Merge Requests - April 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/04/
---

#### Week 17

- **Kate - [Change terminal actions icons and text](https://invent.kde.org/utilities/kate/-/merge_requests/376)**<br />
Request authored by [Felipe Kinoshita](https://invent.kde.org/fhek) and merged after 3 days.

- **Kate - [Remove dead lsp semantic highlighting code](https://invent.kde.org/utilities/kate/-/merge_requests/378)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [remove filtering code, unused](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/140)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Try to fix of a rare crash with Ctrl + Click in LSP](https://invent.kde.org/utilities/kate/-/merge_requests/377)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix leaks in KateConfigDialog](https://invent.kde.org/utilities/kate/-/merge_requests/372)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Adjust file history diff display](https://invent.kde.org/utilities/kate/-/merge_requests/373)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Make dependency on KI18n and KTextWidgets explicit](https://invent.kde.org/utilities/kate/-/merge_requests/374)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [Make KTextWidgets dependency explicit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/141)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

#### Week 16

- **Kate - [Avoid choosing false compile_commands.json file](https://invent.kde.org/utilities/kate/-/merge_requests/371)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [project clazy tool: Fix clazy plugin in case build dir is outside of source](https://invent.kde.org/utilities/kate/-/merge_requests/370)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [addons: Add missing Qt5Concurrent to project and search](https://invent.kde.org/utilities/kate/-/merge_requests/369)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged at creation day.

- **Kate - [Project closing](https://invent.kde.org/utilities/kate/-/merge_requests/355)**<br />
Request authored by [Krzysztof Stokop](https://invent.kde.org/mynewnickname) and merged after 35 days.

- **KTextEditor - [remove completion config - hidden since years](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/139)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Port KLineEdit to QLineEdit where the former&#x27;s features aren&#x27;t used](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/138)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

#### Week 15

- **KSyntaxHighlighting - [systemd unit: update to systemd v248](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/193)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 10 days.

- **kate-editor.org - [Use hugoi18n](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/31)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after 2 days.

- **KSyntaxHighlighting - [add support for Godot&#x27;s gd-scripts](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/194)**<br />
Request authored by [Mario Aichinger](https://invent.kde.org/aichingm) and merged at creation day.

- **KTextEditor - [Fix dragging when folding stuff is around](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Don&#x27;t insert null QActions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/135)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

#### Week 14

- **KTextEditor - [Attempt fix crash on dragging](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/134)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix incremental selection-only search](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/128)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 15 days.

- **Kate - [Bash lsp added](https://invent.kde.org/utilities/kate/-/merge_requests/366)**<br />
Request authored by [Daniele Scasciafratte](https://invent.kde.org/dscasciafratte) and merged after one day.

- **kate-editor.org - [Make better use of title and description](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/30)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [Downgrade lsp semantic highlighting warnings](https://invent.kde.org/utilities/kate/-/merge_requests/367)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Update Readme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/192)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [[applets/preview] Port from KMimeTypeTrader to KParts::PartLoader](https://invent.kde.org/utilities/kate/-/merge_requests/363)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **Kate - [Fix some build warnings](https://invent.kde.org/utilities/kate/-/merge_requests/365)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 13

- **Kate - [Fix shortcut display for multi-key shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/360)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Quick open improvements](https://invent.kde.org/utilities/kate/-/merge_requests/314)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 30 days.

- **KTextEditor - [Fix sizing of completion tree](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/116)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 26 days.

- **KTextEditor - [Make completionWidget border less bright and more like Kate::LSPTooltip](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/132)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: (#5) fix SubShell context that starts as an arithmetic evaluation](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/190)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Doxygen: fix Word style when the word is at the end of line](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/191)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Fix renamed file is not openable directly](https://invent.kde.org/utilities/kate/-/merge_requests/362)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Port from KToolInvocation to KIO::CommandLauncherJob](https://invent.kde.org/utilities/kate/-/merge_requests/361)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [LaTeX: allow Math env within another Math env](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/189)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Add the real blog about the git-blame plugin](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/29)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

