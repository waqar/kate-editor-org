---
title: Merge Requests - August 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/08/
---

#### Week 36

- **KSyntaxHighlighting - [rename &#x27;Default&#x27; to &#x27;Breeze Light&#x27;](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/52)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Varnish, Vala &amp; TADS3: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/51)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Proposal to improve the Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/50)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 35

- **KSyntaxHighlighting - [Add Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/49)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Ruby/Rails/RHTML: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/48)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [LDIF, VHDL, D, Clojure &amp; ANS-Forth94: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/47)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [ASP: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/46)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Various examples](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/45)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged after one day.

- **KSyntaxHighlighting - [Some optimizations on the loading of xml files and Rule::isWordDelimiter function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/38)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [SELinux CIL &amp; Scheme: update bracket colors for dark themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/44)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [POV-Ray: use default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/43)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Test dark highlighting mode, too](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/39)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Django example](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/42)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged at creation day.

- **KSyntaxHighlighting - [CMake: fix illegible colors in dark themes and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/41)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 34

- **KTextEditor - [Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/19)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Extend R syntax highlighting test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/40)**<br />
Request authored by [Momo Cao](https://invent.kde.org/momocao) and merged at creation day.

- **KTextEditor - [autotests: skip crashing tests if Qt doesn&#x27;t have the fix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/18)**<br />
Request authored by [David Faure](https://invent.kde.org/dfaure) and merged after one day.

- **KSyntaxHighlighting - [BrightScript: Add exception syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/35)**<br />
Request authored by [Daniel Levin](https://invent.kde.org/dlevin) and merged after 2 days.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions (Part 3)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/36)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [R Script: use default color style and small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/37)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KTextEditor - [API dox: use doxygen @warning tag to highlight warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/17)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Do not style internal code comments with doxygen-triggering brackets /** */](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/16)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Vimode: Make numbered registers and the small delete register behave more similar to vim](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/15)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Remove obsolete COPYING files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/14)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions (Part 2)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/32)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Remove obsolete COPYING files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/34)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Modelines: remove LineContinue rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/33)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 33

- **KSyntaxHighlighting - [Assembly languages: several fixes and more context highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/30)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Optimization: check if we are on a comment before using ##Doxygen which contains several RegExpr](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/31)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Make &quot;search for selection&quot; search if there is no selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/13)**<br />
Request authored by [Simon Persson](https://invent.kde.org/persson) and merged at creation day.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/29)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 3 days.

- **KSyntaxHighlighting - [ColdFusion: use the default color style and replace some RegExpr rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/28)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 3 days.

- **Kate - [Port away from KMimeTypeTrader](https://invent.kde.org/utilities/kate/-/merge_requests/97)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KTextEditor - [Port the search interface from QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/7)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 14 days.

- **KTextEditor - [Vimode: Implement append-copy (Append to register instead of overwriting)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/11)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [KTextEditor: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/12)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Use angle brackets for context information](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/27)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Syntax-Highlighting: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/25)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Revert removal of byte-order-mark](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/26)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [xslt: change color of XSLT tags](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/24)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [txt2tags: improvements and fixes, use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/22)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Ruby, Perl, QML, VRML &amp; xslt: use the default color style and improve comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/23)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [replace some Regexp with .*$ and add syntax tests](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/18)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 19 days.

#### Week 32

- **KTextEditor - [Speed up a *lot* loading large files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/8)**<br />
Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava) and merged after 5 days.

- **KTextEditor - [Add a text Zoom indicator](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/10)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Show a preview of the matching open bracket&#x27;s line (similar to the Arduino IDE)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/6)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 31 days.

- **KTextEditor - [Use consitently categorized logged warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/9)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

#### Week 31

- **Kate - [Highlight documentation: improve description of attributes of the comment elements](https://invent.kde.org/utilities/kate/-/merge_requests/96)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KTextEditor - [Allow more control over invocation of completion models when automatic invocation is not used.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/5)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 35 days.

- **KSyntaxHighlighting - [create StateData on demand](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/21)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Add support for the R language server](https://invent.kde.org/utilities/kate/-/merge_requests/95)**<br />
Request authored by [Luca Beltrame](https://invent.kde.org/lbeltrame) and merged after 3 days.

- **KSyntaxHighlighting - [Scheme: add datum comment, nested-comment and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/15)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [Mathematica: some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/19)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 8 days.

- **KSyntaxHighlighting - [Doxygen: fix some errors ; DoxygenLua: starts only with --- or --!](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/17)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [AutoHotkey: complete rewrite](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/16)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [Add syntax highlighting for Nim](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/20)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 6 days.

