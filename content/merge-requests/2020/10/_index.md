---
title: Merge Requests - October 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/10/
---

#### Week 44

- **Kate - [Add support for the Zig language server](https://invent.kde.org/utilities/kate/-/merge_requests/117)**<br />
Request authored by [Matheus Catarino](https://invent.kde.org/catarino) and merged after 5 days.

- **KTextEditor - [KateModeMenuList: remove special margins for Windows](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/39)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Add some more boolean values to `cmake.xml`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/93)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Fix: CMake syntax now marks `1` and `0` as special boolean values](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/95)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Include Modelines rules in files where Alerts has been addded](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/94)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Misc: Add `SPDX-FileContributor` to `kateschema2theme`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/89)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 5 days.

- **KSyntaxHighlighting - [Solarized themes: improve separator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/92)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [Improvement: Updates for CMake 3.19](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/91)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **KSyntaxHighlighting - [Add support for systemd unit files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/86)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 5 days.

- **Kate - [Fix: Allow to `KActionSelector` @ filesystem browser config page to stretch vertically](https://invent.kde.org/utilities/kate/-/merge_requests/118)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [angle bracket matching](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/38)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

#### Week 43

- **Kate - [filetree plugin: make filter case insensitive](https://invent.kde.org/utilities/kate/-/merge_requests/114)**<br />
Request authored by [Andreas Hartmetz](https://invent.kde.org/ahartmetz) and merged after 13 days.

- **KSyntaxHighlighting - [Feature: Allow multiple `-s` options for `kateschema2theme` tool](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/88)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Add various tests to the `kateschemsa2theme` converter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/87)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [kateschema2theme: Add a Python tool to convert old schema files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/85)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **Kate - [Correct a typo in README.md](https://invent.kde.org/utilities/kate/-/merge_requests/116)**<br />
Request authored by [Felix Yan](https://invent.kde.org/felixonmars) and merged at creation day.

- **KSyntaxHighlighting - [Decrease opacity in separator of Breeze &amp; Dracula themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/83)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Update README with &quot;Color theme files&quot; section](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/84)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Fix: Use `KDE_INSTALL_DATADIR` when install syntax files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/81)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

#### Week 42

- **Kate - [kate: switch to recently used document when closing one](https://invent.kde.org/utilities/kate/-/merge_requests/115)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Fix memory leak in KateMessageLayout](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/36)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

- **KSyntaxHighlighting - [fix rendering of --syntax-trace=region with multiple graphics on the same offset](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/80)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **KSyntaxHighlighting - [fix some issues of fish shell](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/79)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 12 days.

#### Week 41

- **Kate - [filetree plugin: make it possible to filter items in the tree view](https://invent.kde.org/utilities/kate/-/merge_requests/113)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Cleanup unused comments in KNSRC file](https://invent.kde.org/utilities/kate/-/merge_requests/112)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [ontheflycheck: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/33)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KTextEditor - [katedocument: port one last QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/34)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KTextEditor - [kateview: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/35)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KSyntaxHighlighting - [Replace some RegExpr by StringDetect and StringDetect by Detect2Chars / DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/78)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

#### Week 40

- **KTextEditor - [Compile without deprecated method against qt5.15](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/32)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **KSyntaxHighlighting - [AppArmor: update highlighting for AppArmor 3.0](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/77)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Documentation fixes: Use more entities, some punctuation](https://invent.kde.org/utilities/kate/-/merge_requests/111)**<br />
Request authored by [Antoni Bella Pérez](https://invent.kde.org/bellaperez) and merged at creation day.

- **KSyntaxHighlighting - [color cache for rgb to ansi256colors conversions (speeds up markdown loading)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/74)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [SELinux: use include keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/75)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [SubRip Subtitles &amp; Logcat: small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/76)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [generator for doxygenlua.xml](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/73)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

