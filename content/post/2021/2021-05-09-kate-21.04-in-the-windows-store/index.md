---
title: Kate 21.04 in the Windows Store
author: Christoph Cullmann
date: 2021-05-09T20:00:00+03:00
url: /post/2021/2021-05-09-kate-21.04-in-the-windows-store/
---

## KDE Gear 21.04 including Kate out in the wild!

[KDE Gear 21.04](https://kde.org/announcements/gear/21.04/) was released some weeks ago.

If you use some distribution like e.g. the rolling release Arch Linux (or Arch derivates like Manjaro), you might already have updated to Kate from that release.

As show in the [21.04 pre-release post](/post/2021/2021-03-29-kate-21.04-feature-preview/), Kate 21.04 really has some nifty new features and improvements on all fronts over 20.12.

## Window Store Update => Done!

Unfortunately, unlike for the Linux (or BSD and alike) distributions, where the hard working packagers do the work for us, for e.g. the Windows Store updates, we must do this on our own.

Naturally, thanks to the nice infrastructure like [Craft](https://community.kde.org/Craft) and the [Binary Factory](https://binary-factory.kde.org), this is no purely manual work.

It more or less just is picking some nightly build and ensuring it properly works.

Thanks to the people in the Kate team, this time, not just me tried the version out (or just me and Hannah .)

And yeah, we found some crashs that need fixing before we could upload the version.
(interesting enough in the KTextEditor framework, that behaves just fine on Unices...)

All these fixes will be in the next KDE Frameworks release, until then, Craft blueprints will contain the needed patches.

Kate 21.04 should now be in the [Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW) for all regions.
If you don't know the box art picture there, head over to our [mascot page](/mascot).

## Git Integration on Windows => There we are!

My most loved new feature (beside the faster search in files & polished quick open) is the nice Git integration we now have.

You want to create commits in Kate? Check.

You want to push stuff in Kate? Check.

You want to create a feature branch in Kate? Check ;=)

You want to do this on Windows => Yeah, that works perfectly as long as the Git client is installed and in your PATH.

![Git Integration on Windows with Kate 21.04](/post/2021/2021-05-09-kate-21.04-in-the-windows-store/images/kate-windows-git-integration.png)

If you use Windows (and you use the store version), I hope this update will help you to use Git with Kate on Windows.

If you want to installed 21.04 in a different way, head to our [Get Kate page](/get-it/), using the Windows Store isn't mandatory at all!

If you use e.g. macOS, yes, the latest stable build will provide naturally all features there, too!

## Windows Store statistics

To conclude this post, let's take a look at the overall store statistics for the KDE e.V. published applications.

Below the overall acquisitions since the applications are in the store, sorted by overall number:

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 95,101 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 95,086 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 22,430 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 11,921 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 5,473 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 5,221 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 659 acquisitions

As you see, both Okular & Kate now nearly have 100,000 acquisitions, I think that is rather nice!

Compared to [my post end of 2020](/post/2020/2020-12-20-kde-ev-windows-store-statistics/), Okular has the first time overtaken Kate in the total acquisitions.

In any case, these numbers show that not just a few people really use our stuff on Windows, nice to see this work is appreciated.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/n8lk26/kate_2104_in_the_windows_store/).
