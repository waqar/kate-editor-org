---
title: Jump to Next/Prev Modified Line
author: Dominik Haumann

date: 2014-02-20T21:27:19+00:00
url: /2014/02/20/jump-to-nextprev-modified-line/
categories:
  - Developers
  - Users
tags:
  - planet

---
In [KDE SC 4.8][1], Kate was extended by the [line modification indicators][2]. These indicators show you what lines currently contain unsaved data, but also lines that were once changed but now are saved to disk:

[<img class="aligncenter size-full wp-image-3205" alt="Modified Lines" src="/wp-content/uploads/2014/02/modified-lines.png" width="430" height="129" srcset="/wp-content/uploads/2014/02/modified-lines.png 430w, /wp-content/uploads/2014/02/modified-lines-300x90.png 300w" sizes="(max-width: 430px) 100vw, 430px" />][3]

With Kate in KDE 4.13, we have two new actions in the Edit menu:

  * Move to Previous Modified Line
  * Move to Next Modified Line

In the screenshot above, moving to the next modified line does nothing, since we are already at the very end of the document. Moving to the previous modified line first goes to line 4, then to line 2, and finally to line 1. By default, no shortcuts are assigned, so if you you want to use this, it makes a lot of sense to configure shortcuts.

Modified and saved lines are treated equally, so maybe we should rather call it &#8220;touched&#8221; lines, since all lines that were touched by the user at some point in the edit history are taken into account.

The rationale behind these two actions is to allow fast navigation in the text that you actually work on. Jumping over hundreds of untouched lines is quite handy.

Hope this is useful :-)

 [1]: /2011/12/21/kate-in-kde-4-8/ "Kate in KDE 4.8"
 [2]: /2011/09/06/line-modification-system/ "Line Modification Indicators"
 [3]: /wp-content/uploads/2014/02/modified-lines.png