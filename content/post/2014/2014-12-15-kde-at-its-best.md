---
title: KDE at its very best!
author: Dominik Haumann

date: 2014-12-15T21:32:53+00:00
url: /2014/12/15/kde-at-its-best/
categories:
  - Developers
  - Users
tags:
  - planet

---
Recently, there were some thoughts on <a title="Where KDE is going" href="http://www.baggerspion.net/2014/12/the-pillars-of-kde-now/" target="_blank">where KDE is going</a>, and  related to that what&#8217;s the driving force behind it in terms of the pillars of KDE. Albeit it is true our development model changed significantly, I&#8217;m not convinced that it&#8217;s all about git.

No, I rather believe that it is the excitement about the KDE that makes it stand out &#8211; KDE as a community if you wish, but also KDE as a software project.

Going back to the late nineties, I was developing small games for DOS (Turpo Pascal, anyone? Snake and Gorillas in QBasic? :-) ) and also for Windows. At around that time, Linux got also a bit more popular so that I finally had a SuSE 6.0 in my hands. I installed it and was able to run KDE 2, iirc (?). It certainly was interesting, but then I also wasn&#8217;t involved in any free software projects, so it also wasn&#8217;t _that_ a big deal.

Still, I started to look into how to develop GUI applications for Linux. Since under Windows I used MFC (oh well, now it&#8217;s out, but you know, I quickly got back on the right track) I found Qt quite nice (for CPoint you had QPoint, for CDialog a QDialog, and so on). As I used KDE in Linux, I started to change small things like an Emoticon preview in Kopete (one of my first contributions?), or some wizards for KDevelop in 2003. These were projects that were fairly easy to compile and run. Still, what might seem so little was a huge success for the following reason:

More or less still being child, getting in touch with C++, KDE, and all the tools around it was completely new. CVS? Never heard about it before, and anyways, what was a version control system? How it worked with the mailing lists. With entering a bug. Compiling kdelibs: It took me more than 2 weeks to succeed (Which btw. to myself proves that even at that time it was really hard for a newbe to compile KDE, just like today). All in all, these were times where I learned _a lot_. I started to read the cvs-commit mailing list (around 400 mails a day, I read them almost all, more than 5 years long).

But that was not yet it. It continued like that for _years_. For instance, understanding how KIO slaves worked was just _amazing_. How all KDE components integrate into and interact with each other. There were a lot of parts where KDE was simply the best in terms of the software technology it provided and created.

> <p style="padding-left: 30px;">
>   To me, this was KDE at its best.
> </p>

In my opinion, KDE followed this route for a long time, also with KDE4. I even say KDE still follows this way today.

But it&#8217;s much harder to get excited about it. Why? Think of yourself as seeing snow for the first time. It&#8217;s just awesome, you&#8217;re excited and can&#8217;t believe how awesome this is. Or maybe also New Years eve with nicely looking fireworks coming. It&#8217;s something you simply can&#8217;t wait for enough. Kind of like a small child&#8230; This is the excitement KDE raised in lots of us. Getting a new KDE release was totally something I wanted. I saw the improvements everywhere. What also helped a lot was the detailed commit digest Derek Kite worked on so hard each week, showing what was going on even with detailed discussions and screenshots (today the KDE commit digest is mostly an auto-generated list of commits, which I already have through the KDE commit filter).

Today, I know all the details. All the KDE technology. Of course, it got even better over time, and certainly still is an immensely powerful technology. But I&#8217;m not that much excited about it anymore.

I believe this in itself is not an issue. For exactly this reason, developers come and go, leaving room for other developers to implement their ideas. It helps the project to stay young and agile.

It is often said, the KDE has grown up. This is certainly a good thing for instance in terms of the KDE e.V. supporting the KDE project as much as possible, or the KDE Free Qt Foundation that helps us to make sure Qt will always be freely available to us, or a strong background in legal issues.

At the same time, it is a very bad thing in terms of getting people excited about KDE. We need developers with freaky ideas who just sit down and implement new features (btw., this is very much true for all free software projects). For instance, why has no one come up with a better KXmlGui concept? I&#8217;m sure it can be done better!

Where does that put us? Is there really no cool stuff in KDE?

Well, the reason for this post is to show that we did not loose what once was cool. In fact, we see it every day. For instance, yesterday I was using Dolphin and had to change a lot between subfolders in the same level (e.g. from some\_folder/foo to some\_folder/bar and so on). I accidentally used the mouse wheel over &#8220;foo&#8221;, and whohooo! You can switch to the next folder just by scrolling with the mouse wheel over the navigation bar. This is immensely useful, and in fact, this is why KDE shines also today, it&#8217;s just not so visible to users and maybe also to developers. You now may say that it&#8217;s just some little detail. But this is exactly it: Yesterday I was totally amazed by how cool this is, just like 10 years back from now&#8230; Therefore, I say, this still is

> <p style="padding-left: 30px;">
>   KDE at its very Best!
> </p>

Getting people excited about KDE is what defines KDE&#8217;s future, not git.

**Edit (imho):** I would like to add something here. When reading these kind of blogs, you may get the impression that KDE is getting a less and less attractive platform, or that KDE is kind of dying. This is absolutely not the case. Quite contrary: With KDE&#8217;s foundation libraries, and applications being about to released on top of the KDE Frameworks 5 libraries, KDE can certainly make the statement that the project and its software will definitely be available and certainly just as strong in 10 years from now. I have absolutely no doubt that you can count on that. And _that_ is a really cool thing only few free software projects can claim! Let&#8217;s talk about it again in 2024 :-)

PS: On a unrelated note, KDE currently runs the <a title="KDE End of Year 2014 Fundraiser" href="https://www.kde.org/fundraisers/yearend2014/" target="_blank">End of Year 2014 Fundraiser</a>. Support is very much welcome!