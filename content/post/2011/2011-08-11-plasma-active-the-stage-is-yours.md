---
title: Plasma Active, the stage is yours
author: Christoph Cullmann

date: 2011-08-11T09:29:27+00:00
url: /2011/08/11/plasma-active-the-stage-is-yours/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Thanks a lot to Intel for passing around the [ExoPC][1] at the AppUp workshop yesterday. Its kind of nice hardware to start developing for Intel based tablets, whereas for normal use, the battery life and weight is kind of problematic. I really like the idea to be able to write nice and shiny Qt applications which run both on MeeGo and Windows and the AppUp store is really open in respect to allowing distribute open source software.

But&#8230;

After playing a bit with it last night (it runs some preloaded [MeeGo][2] image with the table UX from Intel), for me it is clear, [Plasma Active][3] is here the way to go.

The pre-installed tablet UX is really, lets say, interesting. Already the startup is kind of weired: your X comes up, you see the ugly X11 default cursor and then, after some flashing around small windows, the UX is there. Some triangle at the right let you go to the main screen. But no, you not drag at it, you need to click. If you then start using the UX, you get kind of scared away by either rendering artefacts, lags or really interesting design concepts (to go back to main screen from an application, you need to touch the left/right upper corner, without any visual hints, kind of confusing, I am dumb, I just rebooted several times to come back to the main screen). In addition, at least this MeeGo version doesn&#8217;t seem to be able to handle multi-touch, which really kills off any fun on using, you can&#8217;t even zoom your pictures (or just the applications don&#8217;t support it, in any case compared to this, Nokia&#8217;s [N9][4] has really a powerful UI).

The good thing is: You need not to use the default MeeGo installation. The hardware is really open in the sense that driver support is no issue (the nice thing with mostly standard Intel hardware). So, go fetch your most recent and flashy Plasma Active image and have fun.

[<img class="size-full wp-image-1146 aligncenter" title="Plasma Active - Contour" src="/wp-content/uploads/2011/08/Contour_AddActivity_Option21.jpg" alt="" width="600" height="337" srcset="/wp-content/uploads/2011/08/Contour_AddActivity_Option21.jpg 600w, /wp-content/uploads/2011/08/Contour_AddActivity_Option21-300x168.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />][5]

[Plasma Active][3] has really a big chance here! Given the current state of the &#8220;default&#8221; MeeGo table UX, Plasma Active is BIG step into the right direction. I hope the Plasma and KWin developers keep on improving this.

A shiny [Wayland][6] based KWin + Plasma combo might really be a game changer here ([presentation about KWin + Wayland][7]).

I hope more people and perhaps companies (like [basysKom][8], which did a real nice job with the above shown [Contour shell][9] for Plasma Active, short article [here][10]) step up and start pushing this effort even more.

Therefore: Thanks again to Intel, I guess they will see more MeeGo apps around soon and lets hope that Plasma Active will get a nice boost, too. Intel will benefit from a better UX in any case!

 [1]: http://en.wikipedia.org/wiki/ExoPC
 [2]: http://www.meego.com
 [3]: http://community.kde.org/Plasma/Active
 [4]: http://en.wikipedia.org/wiki/Nokia_N9
 [5]: /wp-content/uploads/2011/08/Contour_AddActivity_Option21.jpg
 [6]: http://wayland.freedesktop.org/
 [7]: http://community.kde.org/images.community/0/01/KWin_Wayland.pdf
 [8]: http://www.basyskom.com/
 [9]: http://community.kde.org/Plasma/Active/Contour
 [10]: http://www.meegoexperts.com/2011/08/video-demo-contour-running-plasma-active-meego-desktop-summit/