---
title: Code Folding Updates
author: Dominik Haumann

date: 2011-07-31T08:22:15+00:00
url: /2011/07/31/code-folding-updates/
categories:
  - Developers
  - Users
tags:
  - planet

---
As you may know the Kate [code folding implementation][1] [gets some love][2] these days. The last days, <a title="Fixing Confused Code Folding" href="https://projects.kde.org/projects/kde/kdebase/kate/repository/revisions/4151628e86aa0bfaa8a3cc49eb97d06503e69638" target="_blank">several bug fixes</a> dropped in by our GSoC student. And the good news is that if all works well, we&#8217;ll be able to backport these fixes so that the code folding is much more robust in our beloved [Kate even in KDE 4.7.x][3]. These changes probably fix several crashes that were around since 2005 as well. I believe the word we&#8217;re looking for is: _owned_! :-)

 [1]: /2011/07/24/gsoc-2011-%e2%80%93-kate-code-folding-%e2%80%93-try-kates-new-code-folding/ "GSoC: Code Folding"
 [2]: /2011/07/26/gsoc-kate-code-folding-more-technical-details/ "GSoC: Code Folding"
 [3]: /2011/07/09/kate-in-kde-4-7/ "Kate in KDE 4.7.1"