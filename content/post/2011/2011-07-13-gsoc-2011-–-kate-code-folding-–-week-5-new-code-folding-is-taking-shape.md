---
title: GSoC 2011 – Kate Code Folding – week 5 (New Code Folding is taking shape)
author: Adrian Lungu

date: 2011-07-13T09:48:23+00:00
url: /2011/07/13/gsoc-2011-–-kate-code-folding-–-week-5-new-code-folding-is-taking-shape/
categories:
  - Developers
  - KDE
  - Users

---
Hey!

The new code folding is taking shape. Actually it is implemented. :)  
The next step will be testing it (and debugging, if necessary) and implementing some new features.  
Also there are some keyboard shortcuts that didn’t worked so well on the previous version and they need to be fixed too.

If you want to help us with the testing, you can download [my clone][1] and play a little with code folding. Also, if you have some ideas about the new features that Kate’s folding should have, let me know. If it is possible and there is time, they will be implemented.  
Greetings,  
Adrian

 [1]: http://quickgit.kde.org/?p=clones%2Fkate%2Flungu%2Fcode_folding.git&a=summary