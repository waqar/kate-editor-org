---
title: KDE Everywhere?
author: Christoph Cullmann

date: 2011-03-08T07:36:29+00:00
url: /2011/03/08/kde-everywhere/
categories:
  - Developers
  - KDE
tags:
  - planet

---
My last post mentioned [Necessitas][1] which provides the means to have Qt on Android.  
Whereas still a early preview release, it already allows you to compile and run Qt applications on Android >= 1.6 without any big hassle and integrates that into QtCreator!

I tried out the SDK in the last days, it is really easy to use and setup, like seen [here][2] and [here (with good video that shows the steps)][3].

What is missing here? KDE :)

I think a nice thing to have would be a port of parts of kdelibs, like the embedded profile or how you name it and providing an app like Ministro in Necessitas that allows a system wide install of this libs.  
Having tried Ministro and the example app from the market, that works like a charm for Qt already. I would love to see that for kdelibs as well. Then really a lot of users are just two clicks away from great KDE apps like the good edu stuff and games which really can fly on phones and even more tablets ;)  
Embedded developers with spare time: Get the fame and port it .P

I doubt Kate itself would make a good appearance on a phone, and I doubt even I would use it there. But for tablets? Who knows, that might be nice for the &#8220;I hack one liners during Fringe&#8221; session in the evening.

 [1]: http://sourceforge.net/p/necessitas/home/
 [2]: http://sourceforge.net/p/necessitas/wiki/How%20to%20install%20Necessitas%20SDK/
 [3]: http://labs.qt.nokia.com/2011/02/28/necessitas/