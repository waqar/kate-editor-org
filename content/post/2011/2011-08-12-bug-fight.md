---
title: Bug Fight!
author: Christoph Cullmann

date: 2011-08-12T11:47:42+00:00
url: /2011/08/12/bug-fight/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Lets fight the bugs (or here bugs fighting each other, from [de.wikipedia][1]).

<p style="text-align: center;">
  <a href="/wp-content/uploads/2011/08/bugsfight.jpg"><img class="aligncenter size-full wp-image-1188" title="Fighting Male Bugs" src="/wp-content/uploads/2011/08/bugsfight.jpg" alt="" width="546" height="376" srcset="/wp-content/uploads/2011/08/bugsfight.jpg 546w, /wp-content/uploads/2011/08/bugsfight-300x206.jpg 300w" sizes="(max-width: 546px) 100vw, 546px" /></a>
</p>

Dominik and I did a nice bug squashing here at [Berlin Desktop Summit 2011][2]. We already met some weeks ago to hunt down bugs in Kate at Saarbrücken, but this additional time really helped a lot. The progress of the last 365 days look quiet promising.

<p style="text-align: center;">
  <a href="/wp-content/uploads/2011/08/katebugs.png"><img class="aligncenter size-full wp-image-1179" title="Changes in Kate Bugs - 365 Days" src="/wp-content/uploads/2011/08/katebugs.png" alt="" width="663" height="115" srcset="/wp-content/uploads/2011/08/katebugs.png 663w, /wp-content/uploads/2011/08/katebugs-300x52.png 300w" sizes="(max-width: 663px) 100vw, 663px" /></a>
</p>

We actually really fixed quiet a lot of them and sorted out no longer valid ones. In addition a lot of wish list items got either done or invalidated. We just can&#8217;t keep wishlist items for any missing feature we have, e.g. for any missing highlighting. It is not manageable. We won&#8217;t close any wishs for valid extensions, but stuff like: i would like a hl xml file for XYZ or I would like a js indenter for XYZ must go away. Either submit a own .xml or .js file for the job or we won&#8217;t get it anyway.

Here a BIG THANKS to all people that actually submitted for example new highlighting files in their wish list items or who pointed to places to get open source highlighting files to add!

It would be really nice to get some help with the bugs/wishs.  
A lot are just very small things to do, but nobody in the Kate team has really a lot time for it.  
Others just take a lot of time to debug and fix, which is not available, either :(

To write a highlighting, we have a real well tutorial [here][3].  
The same holds for indentation javascripts, look [here][4].  
If the tutorial lack anything, we are really happy about any update for them!

If you like Kate and use it and have any clue about developing with C++ (and a bit knowledge about Qt/KDE), please step up and help us to fix the remaining issues. Any help is welcome and on any recent distribution you can build Kate in no time with [this][5] tutorial. You need no bleeding edge KDE, 4.5/6 should be ok, for sure 4.7 will work, too!

Please all interested people, head over to [Kate&#8217;s bugs][6] and [Kate&#8217;s wishs][7] and help to get rid of them!

But please have a hearth for the real bugs, like the above shown [Lucanus cervus][8], they are quiet rare, at least here in Germany!

 [1]: http://de.wikipedia.org/wiki/Hirschk%C3%A4fer
 [2]: https://desktopsummit.org/
 [3]: /2005/03/24/writing-a-syntax-highlighting-file/
 [4]: /2009/10/29/extending-kate-with-scripts/
 [5]: /get-it/
 [6]: http://bugs.kde.org/buglist.cgi?product=kate&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor
 [7]: http://bugs.kde.org/buglist.cgi?product=kate&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist
 [8]: http://en.wikipedia.org/wiki/Lucanus_cervus