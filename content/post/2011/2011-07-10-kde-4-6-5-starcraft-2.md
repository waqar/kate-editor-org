---
title: 'KDE 4.6.5 > StarCraft 2'
author: Christoph Cullmann

date: 2011-07-10T16:47:23+00:00
url: /2011/07/10/kde-4-6-5-starcraft-2/
pw_single_layout:
  - "1"
categories:
  - KDE
  - Users
tags:
  - planet

---
I normally enjoy StarCraft 2 in the evening a bit or like now, if it is raining and I have no fun in any more real work.

But just now and some days ago, it has again proven, KDE > StarCraft 2.

Just again, StarCraft 2 (played with wine, thanks a lot wine team, you do a great job!) got sluggish and then terminated.  
A quick dmesg call shows the cause.  
Like last time, the OOM killer got me.

Now, first I thought: What, this stupid game uses more than my 4 GB of RAM? (I have no swap, have only one ssd)  
But now, the reason is much easier: KDE has beaten StarCraft 2&#8230;. (even if we call it KDE SC, it still not loves SC 2 :)

Looking at my process table, is is just clear: StarCraft 2 is the biggest memory user, that is just normal, around 2.5 GB, at peak.  
But no reason to crash, or? With 4 GB.  
But wait, I let my kontact run the background, big failure.

My stats here:

Kontact and co, just only taken the > 50 MB RSS offenders, there are lot of other smaller processes running around:

cullmann 1452 2.1 10.7 1243552 436228 ? Sl 11:05 9:33 /usr/bin/kontact  
cullmann 1132 0.5 9.6 813136 391996 ? S 10:38 2:33 /usr/bin/akonadi\_nepomuk\_email\_feeder &#8211;identifier akonadi\_nepomuk\_email\_feeder  
cullmann 1127 2.6 1.3 550532 54860 ? Sl 10:38 12:13 /usr/bin/akonadi\_imap\_resource &#8211;identifier akonadi\_imap\_resource_0  
cullmann 1060 2.0 2.8 481124 114288 ? Sl 10:38 9:34 /usr/bin/mysqld &#8211;defaults-file=/home/cullmann/.local/share/akonadi//mysql.conf &#8211;datadir=/h

Summa around 1 GB, with the smaller ones.

+ my friend the semantic desktop :/  
cullmann 1104 41.6 2.1 168752 88320 ? SNl 10:38 193:59 /usr/bin/virtuoso-t +foreground +configfile /tmp/virtuoso_hX1095.ini +wait

The planet is no place for bugs, I know, but anyway, this is no bug report. What should I report at all? That just using KDE + Kontact eats >> 1 GB of my RAM and virtuoso around 40% of my CPU? And yes, it even uses that with Kontact shutdown and all other apps closed&#8230; And no, I have no indexing active, that OOM&#8217;d me last time, even before Kontact + Akonadi on :/

I kind of think, we need to improve in that area, really.  
Beside, in the old days, if Kontact did take too much RAM, I just closed it, now, still the akonadi parts + mysql stay alive, with around 1/2 GB memory usage :/  
Guess need to kill them hard before starting to play :)

A lot of persons at work switched back to GNOME or whatever, because we can&#8217;t get rid of these problems. Either kdeinit eats our CPU (with Ubuntu Natty) or nepomuk/strigi/akonadi and Co. eat the memory + fill our NFS homes :/ And no, we don&#8217;t do bug reports for this, as these are not reproducable problems. (Beside the memory usage I guess and that virtuoso runs amok, but that are known issues if you look at our bug tracker)

Good side of things: In that light, Kate&#8217;s memory usage is kind of non-existing in comparison :P

I will continue to use KDE for sure and try to be more clever and shut down Kontact and co before playing (and turn of nepomuk completly, even if I get here and then a error message because of this) or I will just buy more RAM, 4GB to ease the pain should be not the biggest issue. Happy enough, at work I have anyway 8GB or more in every workstation because of our static analysis software needing it ;)