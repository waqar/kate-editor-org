---
title: Line Modification System
author: Christoph Cullmann

date: 2011-09-06T18:31:54+00:00
url: /2011/09/06/line-modification-system/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Some days ago, Dominik implemented a line modification system in the KatePart.  
For all who don&#8217;t know what that means at all (like me before the DS 2011), here some screenshots.

First, starting with a fresh loaded file:  
[<img src="/wp-content/uploads/2011/09/no_edit-e1315333531831.png" alt="" title="Fresh unedited file" width="560" height="128" class="aligncenter size-full wp-image-1433" srcset="/wp-content/uploads/2011/09/no_edit-e1315333531831.png 560w, /wp-content/uploads/2011/09/no_edit-e1315333531831-300x68.png 300w" sizes="(max-width: 560px) 100vw, 560px" />][1]

Now, lets write some lines:

[<img src="/wp-content/uploads/2011/09/text_edited-e1315333516826.png" alt="" title="Some text written" width="560" height="146" class="aligncenter size-full wp-image-1434" srcset="/wp-content/uploads/2011/09/text_edited-e1315333516826.png 560w, /wp-content/uploads/2011/09/text_edited-e1315333516826-300x78.png 300w" sizes="(max-width: 560px) 100vw, 560px" />][2]

Next, after saving:  
[<img src="/wp-content/uploads/2011/09/text_edited_and_saved-e1315333486897.png" alt="" title="Text edited and then saved" width="559" height="141" class="aligncenter size-full wp-image-1435" srcset="/wp-content/uploads/2011/09/text_edited_and_saved-e1315333486897.png 559w, /wp-content/uploads/2011/09/text_edited_and_saved-e1315333486897-300x75.png 300w" sizes="(max-width: 559px) 100vw, 559px" />][3]

Now, lets change some stuff again:  
[<img src="/wp-content/uploads/2011/09/text_edited_again-e1315333464920.png" alt="" title="Again text edited after save" width="560" height="152" class="aligncenter size-full wp-image-1436" srcset="/wp-content/uploads/2011/09/text_edited_again-e1315333464920.png 560w, /wp-content/uploads/2011/09/text_edited_again-e1315333464920-300x81.png 300w" sizes="(max-width: 560px) 100vw, 560px" />][4]

Awesome :)

First I thought: Why to hell do I need such a feature? But after using it some days now, I think it is really VERY useful.

Yesterday I worked in parallel on code that generates integer linear programs as CPLEX files and read the output files (which are > 100000 lines) to find regressions.  
I had to manually tweak the ILP files a bit to reveal the reason of my issues and it was just cool to exactly know even after save, which lines in the ILP I touched.  
Normally I would have written some comments like \changed&#8230; above my modified lines in the output files, but yeah, KatePart does track such stuff now automatically ;)

Even for the much less big source files, it was nice to see:  
Oh, I just tweaked that few C++ lines and it did remove the issue, maybe I should look at the lines below/above of that lines, too, for more possible problems?

Therefor: Thanks to Dominik for this nifty feature, I hope others appreciate it, too!

If you want this feature now: Use the guide at kate-editor.org&#8217;s [Get It!][5].

 [1]: /wp-content/uploads/2011/09/no_edit-e1315333531831.png
 [2]: /wp-content/uploads/2011/09/text_edited-e1315333516826.png
 [3]: /wp-content/uploads/2011/09/text_edited_and_saved-e1315333486897.png
 [4]: /wp-content/uploads/2011/09/text_edited_again-e1315333464920.png
 [5]: /get-it/