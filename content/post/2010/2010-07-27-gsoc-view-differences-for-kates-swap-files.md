---
title: GSoC – View differences for Kate’s swap files
author: dianat

date: 2010-07-27T09:16:07+00:00
url: /2010/07/27/gsoc-view-differences-for-kates-swap-files/
categories:
  - Developers
tags:
  - planet

---
Hello,

<div>
  As I stated in a previous <a title="post" href="/2010/07/12/gsoc-swap-files-for-kate/" target="_blank">post</a>, the swap file feature for Kate is almost done. Back then, the view differences feature wasn&#8217;t ready, but now we have a basic implementation of it.
</div>

<div>
</div>

<div>
  So now, by pressing the &#8220;View changes&#8221; button, a new KProcess is created, which receives as command line arguments the &#8216;diff&#8217; program and the two files to be compared. One file is the original file on the disk, and the other one is represented by the recovered data read from the standard input. Then, Kompare launches, and there you can see the differences.
</div>

<div>
</div>

<div>
  But sadly, at the moment you can&#8217;t merge the changes or some of them through Kompare, but I&#8217;m working on it. All you can do is see the differences and decide whether you want to recover the lost data or not. Close Kompare, and then press the &#8220;Recover&#8221; button or the &#8220;Discard&#8221; one, depending on what you want to do.
</div>

<div>
</div>