---
title: kate-editor.org shaping up
author: Christoph Cullmann

date: 2010-07-10T08:27:28+00:00
url: /2010/07/10/kate-editor-org-shaping-up/
categories:
  - Common
tags:
  - planet

---
kate-editor.org now contains all articles from the old page still applicable to Kate in KDE 4 and in addition all blog entries of Dominik which are Kate centric.

Beside that it will aggregate the blog entries of Milian for Kate ;)

Still, a lot of work is needed. We already got an offer for help to beautify the page by a web designer, still content writers are missing. You have a nice short post about how to use Kate best? You have some howto? Just contact [us][1] or me in [private][2]. We can either give you an account on the page or just add your content if you like that more.

 [1]: mailto:kwrite-devel@kde.org
 [2]: mailto:cullmann@kde.org