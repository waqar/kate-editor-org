---
title: Tree View plugin introduction
author: Thomas Fjellstrom

date: 2010-09-10T01:07:36+00:00
excerpt: "New document list plugin for Kate, KDE's Advanced Text Editor."
url: /2010/09/10/tree-view-plugin-introduction/
categories:
  - Users
tags:
  - planet

---
Kate has been my favorite editor for some time now. And only recently has it not been able to do something I asked it do to. I was working on a rather large project in Perl when things started getting confusing. The classic file list had 50+ files in it, some of them have the same file name, so would have (1) or (2) or even (4) after the file name, and while thats handy, it gets confusing pretty quickly.  
<!--more-->

So I spent a week to try and bring order to the chaos. And this is what I came up with.

[<img class="alignnone size-medium wp-image-667" src="/wp-content/uploads/2010/09/kate-treeview1-300x170.png" alt="" width="300" height="170" srcset="/wp-content/uploads/2010/09/kate-treeview1-300x170.png 300w, /wp-content/uploads/2010/09/kate-treeview1.png 845w" sizes="(max-width: 300px) 100vw, 300px" />][1]

Now I can tell people are thinking, but Thomas, that doesn&#8217;t look so confusing.. Granted.

Here&#8217;s what my larger project looks like with the file list:

[<img class="alignnone size-medium wp-image-668" src="/wp-content/uploads/2010/09/kate-filelist1-300x164.png" alt="" width="300" height="164" srcset="/wp-content/uploads/2010/09/kate-filelist1-300x164.png 300w, /wp-content/uploads/2010/09/kate-filelist1.png 932w" sizes="(max-width: 300px) 100vw, 300px" />][2]

I think anyone will have a hard time working with _that_ unholy mess. After trying to manage that for more than a couple of days I sat down and decided to write the tree plugin. Heres what the mess looks like now:

[<img src="/wp-content/uploads/2010/09/kate-treeview2-300x164.png" alt="" width="300" height="164" class="alignnone size-medium wp-image-674" srcset="/wp-content/uploads/2010/09/kate-treeview2-300x164.png 300w, /wp-content/uploads/2010/09/kate-treeview2.png 932w" sizes="(max-width: 300px) 100vw, 300px" />][3]

Is that not a lot better?

Some of you reading this might have noticed that it isn&#8217;t creating a full file system tree. That is it isn&#8217;t making &#8220;/&#8221; or &#8220;C:\&#8221; the root of the tree. Indeed. It uses a couple tricks to make sure the tree isn&#8217;t cluttered with directories you really don&#8217;t care about. If people are interested I can explain more about how it does what it does in a more technical post.

I&#8217;ve heard a few people ask if this shouldn&#8217;t just outright replace the existing file list. The Kate developers don&#8217;t see why not, but I have my doubts that it is a proper &#8220;one size fits all&#8221; solution. As far as I can tell, it is _impossible_ to make the tree behave in a fully sane and expected manner. In some cases it won&#8217;t merge some folders you think it should, but (IMO) trying to make the tree behave any &#8220;better&#8221; would make it less useful (at least to me). I&#8217;m interested in what people think, is it &#8220;good enough&#8221; to become the default document list, or even replace the existing one outright?

I&#8217;ve committed the plugin to KDE svn, and just uploaded to kde-apps if anyone is interested in giving it a try. I&#8217;ve tested it with Kate/KDE 4.4, 4.5, and Kate from trunk. Please let me know what you think, and tell me if you have any problems or concerns! Thanks :)

p.s. I&#8217;m new to this whole blogging thing, please be gentle ;)

 [1]: /wp-content/uploads/2010/09/kate-treeview1.png
 [2]: /wp-content/uploads/2010/09/kate-filelist1.png
 [3]: /wp-content/uploads/2010/09/kate-treeview2.png