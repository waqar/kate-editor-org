---
title: KateSQL, a new plugin for Kate
author: Marco Mentasti

date: 2010-07-29T21:45:22+00:00
url: /2010/07/29/katesql-a-new-plugin-for-kate/
categories:
  - Developers
  - Users
tags:
  - planet

---
Hello,

today i will show you a new plugin for Kate, called **KateSQL**.  
As you may have guessed, it brings to Kate the basic features of an SQL client, allowing you to open connections, execute queries, and display result data from SELECT statements or stored procedures.  
Since this plugin makes an extreme use of the **Qt Sql module**, most of database drivers are supported..

Said this, let me explain how it works..

<p style="text-align: center">
  <a href="/wp-content/uploads/2010/07/katesql-screenshot-1.png"><img class="size-full wp-image-496 aligncenter" src="/wp-content/uploads/2010/07/katesql-screenshot-1.png" alt="Kate MainWindow with KateSQL widgets" width="661" height="454" srcset="/wp-content/uploads/2010/07/katesql-screenshot-1.png 1101w, /wp-content/uploads/2010/07/katesql-screenshot-1-300x206.png 300w, /wp-content/uploads/2010/07/katesql-screenshot-1-1024x704.png 1024w" sizes="(max-width: 661px) 100vw, 661px" /></a>
</p>

First of all, you have to create a new db connection through [a simple wizard][1], specifying driver, connection parameters (hostname, username, password, etc&#8230;), and a descriptive name, that KateSQL will use as identifier.  
Done this, what you need to do is just select the query text in the editor and press F5 (or your preferred shortcut). The whole text will be executed through the selected connection.

For SQL output, two toolbox are disposed in the bottom area:

  * The first will show messages returned from the server (errors in red, others in green, like number of affected rows).
  * The second contains a table view with a custom model associated, to show resultsets of a query. This custom model does nothing more than a **QSqlQueryModel**, only provides colors and formatting for each cell..

Ah, about this, my last commit implements a configuration widget that let you choose colors and font styles for text fields, numbers, blobs, nulls, booleans and dates&#8230; cool, yeah? :)

[<img class="aligncenter size-full wp-image-497" src="/wp-content/uploads/2010/07/katesql-screenshot-2.png" alt="KateSQL Configuration Dialog" width="352" height="192" srcset="/wp-content/uploads/2010/07/katesql-screenshot-2.png 705w, /wp-content/uploads/2010/07/katesql-screenshot-2-300x163.png 300w" sizes="(max-width: 352px) 100vw, 352px" />][2]

Last but not least, on the left panel you can find a basic and useful **schema browser**, that show the tree schema of the database connection currently selected. With this tree widget you can browse through tables, system tables and views, up to individual fields. obviously, primary key fields are distinguished by the classic yellow key icon.

Currently, there are few problems with multiple query handling.. Some engines doesn’t supports it natively, others can receive queries separated by a semicolon, but the QSqlQueryModel can handle only one resultset at time.. probably the best solution is to parse the text, split queries, and execute them separately.. Surely, this feature will be implemented soon.  
Stay tuned!

Of course, if you want to [help us][3] with development, you are welcome!

 [1]: /wp-content/uploads/2010/07/katesql-wizard-screenshot.png
 [2]: /wp-content/uploads/2010/07/katesql-screenshot-2.png
 [3]: /join-us/