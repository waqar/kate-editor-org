---
title: Multi-Line Text Editing in Kate
author: Dominik Haumann

date: 2013-09-09T20:51:04+00:00
url: /2013/09/09/multi-line-text-editing-in-kate/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Right after the [vim blog][1] and the &#8216;[what&#8217;s new in 4.11][2]&#8216; blog we have another feature that was <a title="Multi-line editing in block selection mode" href="https://bugs.kde.org/show_bug.cgi?id=211251" target="_blank">long requested</a> and finally is available in 4.12: Multi-line editing in block selection mode.

<img class="aligncenter size-full wp-image-2869" title="Multiline Text Editing in Kate (KDE SC 4.12)" src="/wp-content/uploads/2013/09/multiline-editing.gif" alt="" width="470" height="220" srcset="/wp-content/uploads/2013/09/multiline-editing.gif 470w, /wp-content/uploads/2013/09/multiline-editing-300x140.gif 300w" sizes="(max-width: 470px) 100vw, 470px" /> 

Needless to say that it also supports undo/redo and pasting text. Kudos go to Andrey Matveyakin for implementing it!

 [1]: /2013/09/06/kate-vim-progress/ "Vim Features"
 [2]: /2013/09/09/kate-in-4-11/ "Kate in 4.11"