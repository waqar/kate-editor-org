---
title: Support KDE via AmazonSmile
author: Dominik Haumann

date: 2018-10-14T20:02:50+00:00
url: /2018/10/14/support-kde-via-amazonsmile/
categories:
  - KDE
  - Users
tags:
  - planet

---
For quite some time, the KDE e.V. &#8211; KDE&#8217;s non-profit organization &#8211; is listed in the AmazonSmile program. On the [AmazonSmile website][1] it says:

> <p style="padding-left: 30px;">
>   AmazonSmile is a website operated by Amazon that lets customers enjoy the same wide selection of products, [&#8230;]. The difference is that when customers shop on AmazonSmile (smile.amazon.com), the AmazonSmile Foundation will donate 0.5% of the price of eligible purchases to the charitable organizations selected by customers.
> </p>

In other words, if you do a lot of shopping on Amazon anyways, make sure to start shopping via [smile.amazon.de][2] and choose the &#8220;KDE e.V.&#8221; as organization. This way you financially support the KDE e.V., even without having to pay more. I am posting this here since I know many people who buy things on Amazon several times a week. Feel free to spread the word & happy shopping :-)

PS: This is not supposed to be advertisement for Amazon. You can also donate to the KDE e.V. directly.

PPS: It seems the KDE e.V. is only available on German Amazon.

 [1]: https://org.amazon.com/
 [2]: https://smile.amazon.de/