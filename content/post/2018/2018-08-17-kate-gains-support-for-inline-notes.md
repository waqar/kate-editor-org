---
title: Kate gains Support for Inline Notes
author: Dominik Haumann

date: 2018-08-17T20:48:13+00:00
url: /2018/08/17/kate-gains-support-for-inline-notes/
categories:
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
Thanks to Michal Srb and Sven Brauch for pioneering the work an a new KTextEditor interface that allows applications like Kate, KDevelop, etc. to display inline notes in a text document. As demo, we quickly prototyped one application to display colors in CSS documents:

<img class="aligncenter size-full wp-image-4249" src="/wp-content/uploads/2018/08/inlinenote.png" alt="" width="789" height="572" srcset="/wp-content/uploads/2018/08/inlinenote.png 789w, /wp-content/uploads/2018/08/inlinenote-300x217.png 300w, /wp-content/uploads/2018/08/inlinenote-768x557.png 768w" sizes="(max-width: 789px) 100vw, 789px" /> 

Clicking on the color rectangle will launch the color chooser:

<img class="aligncenter size-full wp-image-4250" src="/wp-content/uploads/2018/08/inlinenote-dialog.png" alt="" width="789" height="572" srcset="/wp-content/uploads/2018/08/inlinenote-dialog.png 789w, /wp-content/uploads/2018/08/inlinenote-dialog-300x217.png 300w, /wp-content/uploads/2018/08/inlinenote-dialog-768x557.png 768w" sizes="(max-width: 789px) 100vw, 789px" /> Choosing a color and clicking OK finally adapts the color in the CSS document:

<img class="aligncenter size-full wp-image-4251" src="/wp-content/uploads/2018/08/inlinenote-final.png" alt="" width="789" height="572" srcset="/wp-content/uploads/2018/08/inlinenote-final.png 789w, /wp-content/uploads/2018/08/inlinenote-final-300x217.png 300w, /wp-content/uploads/2018/08/inlinenote-final-768x557.png 768w" sizes="(max-width: 789px) 100vw, 789px" /> 

The code for this is just a demo and looks as follows:

<pre style="font-size: 11px;"><b>class</b> NoteProvider : <b>public</b> KTextEditor::InlineNoteProvider {
<b>public</b>:
    <span style="color: #0095ff;">QVector</span>&lt;<span style="color: #0057ae;">int</span>&gt; inlineNotes(<span style="color: #0057ae;">int</span> line) <span style="color: #0057ae;">const</span> <b>override</b>
    {
        <b>if</b> (line == <span style="color: #b08000;">1</span>) <b>return</b> { <span style="color: #b08000;">29</span> };
        <b>if</b> (line == <span style="color: #b08000;">11</span>) <b>return</b> { <span style="color: #b08000;">29</span> };
        <b>if</b> (line == <span style="color: #b08000;">12</span>) <b>return</b> { <span style="color: #b08000;">29</span> };
        <b>if</b> (line == <span style="color: #b08000;">13</span>) <b>return</b> { <span style="color: #b08000;">29</span> };

        <b>return</b> {};
    }

    <span style="color: #0095ff;">QSize</span> inlineNoteSize(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note) <span style="color: #0057ae;">const</span> <b>override</b>
    {
        <b>return</b> <span style="color: #0095ff;">QSize</span>(note.lineHeight(), note.lineHeight());
    }

    <span style="color: #0057ae;">void</span> paintInlineNote(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note, <span style="color: #0095ff;">QPainter</span>& painter) <span style="color: #0057ae;">const</span> <b>override</b>
    {
        <span style="color: #0057ae;">const</span> <b>auto</b> line = note.position().line();
        <span style="color: #0057ae;">const</span> <b>auto</b> color = <span style="color: #0095ff;">QColor</span>(note.view()-&gt;document()-&gt;text({line, <span style="color: #b08000;">22</span>, line, <span style="color: #b08000;">29</span>}));
        painter.setPen(color);
        painter.setBrush(color.lighter(<span style="color: #b08000;">150</span>));
        painter.drawRoundedRect(<span style="color: #b08000;">1</span>, <span style="color: #b08000;">1</span>, note.width() - <span style="color: #b08000;">2</span>, note.lineHeight() - <span style="color: #b08000;">2</span>, <span style="color: #b08000;">2</span>, <span style="color: #b08000;">2</span>);
    }

    <span style="color: #0057ae;">void</span> inlineNoteActivated(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note, <span style="color: #0095ff;">Qt::MouseButtons</span> buttons, <span style="color: #0057ae;">const</span> <span style="color: #0095ff;">QPoint</span>& globalPos) <b>override</b>
    {
        <span style="color: #0057ae;">const</span> <span style="color: #0057ae;">int</span> line = note.position().line();
        <span style="color: #0057ae;">const</span> <b>auto</b> oldColor = <span style="color: #0095ff;">QColor</span>(note.view()-&gt;document()-&gt;text({line, <span style="color: #b08000;">22</span>, line, <span style="color: #b08000;">29</span>}));
        <span style="color: #0057ae;">const</span> <b>auto</b> newColor = <span style="color: #0095ff;">QColorDialog::getColor</span>(oldColor);
        note.view()-&gt;document()-&gt;replaceText({line, <span style="color: #b08000;">22</span>, line, <span style="color: #b08000;">29</span>}, newColor.name(<span style="color: #0095ff;">QColor::HexRgb</span>));
    }

    <span style="color: #0057ae;">void</span> inlineNoteFocusInEvent(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note, <span style="color: #0057ae;">const</span> <span style="color: #0095ff;">QPoint</span>& globalPos) <b>override</b>
    {} <span style="color: #898887;">// unused in this example</span>

    <span style="color: #0057ae;">void</span> inlineNoteFocusOutEvent(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note) <b>override</b>
    {} <span style="color: #898887;">// unused in this example</span>

    <span style="color: #0057ae;">void</span> inlineNoteMouseMoveEvent(<span style="color: #0057ae;">const</span> KTextEditor::InlineNote& note, <span style="color: #0057ae;">const</span> <span style="color: #0095ff;">QPoint</span>& globalPos) <b>override</b>
    {} <span style="color: #898887;">// unused in this example</span>
};

<span style="color: #898887;">// later in code:</span>
<b>auto</b> provider = <b>new</b> NoteProvider();
view-&gt;registerInlineNoteProvider(provider);
<span style="color: #898887;">// final cleanup</span>
view-&gt;unregisterInlineNoteProvider(provider);
</pre>

As you can see, it&#8217;s actually not much code at all: We have to derive a class from [KTextEditor::InlineNoteProvider][1], and then register an instance of our Note Provider in the [KTextEditor::View][2]. In a next step, we implement the inlineNotes(), inlineNoteSize(), and the paintInlineNote() functions to get basic visual drawing at the desired location. The above code is just a tech-demo, since it uses hard-coded lines and color positions. Additionally, one can also track mouse events (unused in the example above). On mouse click, we open the QColorDialog to let the user choose a new color.

To give more examples of what&#8217;s possible, the initial Phabricator review requests contained many other interesting examples (the examples were really implemented). From [review request D12662][3]:

<figure id="attachment_4256" aria-describedby="caption-attachment-4256" style="width: 695px" class="wp-caption aligncenter"><img class="wp-image-4256 size-full" src="/wp-content/uploads/2018/08/kdevelop.png" alt="" width="695" height="630" srcset="/wp-content/uploads/2018/08/kdevelop.png 695w, /wp-content/uploads/2018/08/kdevelop-300x272.png 300w" sizes="(max-width: 695px) 100vw, 695px" /><figcaption id="caption-attachment-4256" class="wp-caption-text">Kate showing additional information for loops and structs.</figcaption></figure>

Or a KDevelop addition that adds a lot of meta information on the current code if desired:

<figure id="attachment_4257" aria-describedby="caption-attachment-4257" style="width: 717px" class="wp-caption aligncenter"><img class="wp-image-4257 size-full" src="/wp-content/uploads/2018/08/inline-note-anim.gif" alt="" width="717" height="961" /><figcaption id="caption-attachment-4257" class="wp-caption-text">KDevelop showing detailed code meta information</figcaption></figure>

We believe this addition to the KTextEditor component has a lot of potential for nice features and plugins. Feel free to use this interfaces starting with KDE Frameworks 5.50. Happy coding! :-)

A big thanks also goes to this year&#8217;s Akademy organizers. Thanks to this event, we could meet up in person and also finalize the [InlineNoteInterface][4], [InlineNoteProvider][1], and [InlineNote][5] class to make it ready for public release. This again shows the importance of the yearly KDE conferences since it enables us to significantly push things forward.

 [1]: https://api.kde.org/frameworks/ktexteditor/html/classKTextEditor_1_1InlineNoteProvider.html
 [2]: https://api.kde.org/frameworks/ktexteditor/html/classKTextEditor_1_1View.html
 [3]: https://phabricator.kde.org/D12662
 [4]: https://api.kde.org/frameworks/ktexteditor/html/classKTextEditor_1_1InlineNoteInterface.html
 [5]: https://api.kde.org/frameworks/ktexteditor/html/classKTextEditor_1_1InlineNote.html