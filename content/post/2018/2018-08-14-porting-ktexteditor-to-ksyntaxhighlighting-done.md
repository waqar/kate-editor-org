---
title: 'Porting KTextEditor to KSyntaxHighlighting => Done :=)'
author: Christoph Cullmann

date: 2018-08-14T16:05:22+00:00
url: /2018/08/14/porting-ktexteditor-to-ksyntaxhighlighting-done/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
During Akademy there was finally enough time to finalize the porting of KTextEditor to KSyntaxHighlighting.

Thanks to the help of Dominik and Volker, the needed extensions to the KSyntaxHighlighting framework were done in no time ;=)

Thanks for that!

The branch for the integration was merged to master yesterday, unit tests look OK and I am using that state now for my normal coding work. Beside minor glitches that should now be corrected, no issues came up until now.

But as with all changes, for sure some regressions slipped in.

If you notice strange behavior of the highlighting in master, please report the issues on bugs.kde.org or on our mailing list (or even better: provide a fix on [phabricator][1]).

For sure there are potential further cleanups and now that we have only one implementation of the highlighting infrastructure, we will be able to move forward with extensions of it much easier.

We [contacted][2] the QtCreator people, to see if we might be able to share a common implementation, as they have an own one at the moment. Hopefully that works out in a nice way and our KDE syntax-highlighting framework gets an even broader user base!

<img class="aligncenter size-medium wp-image-4142" src="/wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg" alt="" width="300" height="74" srcset="/wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg 300w, /wp-content/uploads/2018/07/going_to_akademy_banner-768x188.jpg 768w, /wp-content/uploads/2018/07/going_to_akademy_banner.jpg 840w" sizes="(max-width: 300px) 100vw, 300px" />

 [1]: https://phabricator.kde.org
 [2]: http://lists.qt-project.org/pipermail/qt-creator/2018-August/007505.html