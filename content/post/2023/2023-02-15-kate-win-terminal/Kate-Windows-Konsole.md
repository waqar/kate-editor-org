---
title: Introducing The Embedded Terminal on Windows
author: Waqar Ahmed
date: 2023-02-15T12:18:00+05:00
url: /post/2023/2023-02-15-kate-win-terminal
---

Kate has been supported on Windows for a long time however, we missed one crucial feature on Windows that made it not as good as Kate on Linux. That feature was the built-in terminal. If you are a developer you might have to use the terminal a lot and having a built-in terminal inside your editor can be really helpful and convenient.

With 23.04, we have filled this gap. After a hectic weekend of 20+ hours of hacking on a borrowed windows machine and sacrificing a lot of other things, Kate now has a built-in terminal on Windows as well. The terminal we used is based on [qtermwidget](https://github.com/lxqt/qtermwidget/) which is a fork of an older version of Konsole and uses [ptyqt](https://github.com/kafeg/ptyqt) for pty. I had to make a lot of modifications to support the new terminal on Windows/Mac/Linux and make it build with Qt6 as well as Qt5 but now it is done and we merged it last night after some testing. There are going to be a lot of bugs for sure, but at least we now have a proper terminal to offer on Windows. Obligatory screenshot below:

![Kate on windows running powershell in the terminal](/post/2023/2023-02-15-kate-win-terminal/images/kate-full-dark-mode.png)

I have tried hard to offer the exact same experience to Windows users that we give Linux users so that no one has to learn anything. I am glad to say that it will be quite similar if not the same. There are a couple of shortcomings, but the rest looks almost the same. It supports multiple tabs (focus the terminal and hit Ctrl+Shift+T), search, colorschemes and opening links.

### Does this mean anything will change for Linux or Mac users?

No. Linux users will still enjoy the latest Konsole. However, if someone doesn't have Konsole installed for whatever reason this new terminal will be activated as a fallback.

### Will Konsole itself work on Windows?

Atm no. However, the good news is that making Konsole work on Windows will probably not be that hard and now we have *some* proof that Konsole *can* work on windows. I have been in touch with one of the Konsole devs about this and we might end up porting Konsole to windows this year :)

Before closing the post, I must say that this is a temporary solution. We don't really want to maintain a full terminal inside the editor as it's a lot of weight to carry. The main fear I have is that none of the active Kate developers use Windows which means that it will be a bit hard to fix the bugs. Let see. The ideal solution for us is that we port Konsole to windows and then reuse that.

Are you a Kate user on Windows? We'd love some feedback.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/1137qnc/kate_introducing_the_embedded_terminal_on_windows/).
