---
title: KDE e.V. Microsoft Store Statistics
author: Christoph Cullmann
date: 2023-04-30T19:19:00+03:00
url: /post/2023/2023-04-30-kde-ev-microsoft-store-statistics/
---

Let's take a look at the current state of the KDE e.V. published applications in the Microsoft Store.

### Last 30 days statistics

Here are the number of acquisitions for the last 30 days (roughly equal to the number of installations, not mere downloads) for our applications:

* [KDE Connect - Enabling communication between all your devices](https://www.microsoft.com/store/apps/9N93MRMSXBF0) - 11,640 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 9,100 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 3,000 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 1,170 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 378 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 339 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 289 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 284 acquisitions

* [NeoChat - Matrix Client](https://www.microsoft.com/store/apps/9PNXWVNRC29H) - 108 acquisitions

A nice stream of new users for our software on the Windows platform.

### Overall statistics

The overall acquisitions since the applications are in the store:

* [KDE Connect - Enabling communication between all your devices](https://www.microsoft.com/store/apps/9N93MRMSXBF0) - 300,250 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 270,068 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 182,744 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 49,403 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 22,879 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 12,319 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 10,453 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 6,410 acquisitions

* [NeoChat - Matrix Client](https://www.microsoft.com/store/apps/9PNXWVNRC29H) - 439 acquisitions

### Help us!

If you want to help to bring more stuff KDE develops on Windows, please contact the maintainers of the stuff you want to help out with.

A guide how to submit stuff later can be found [on develop.kde.org](https://develop.kde.org/docs/packaging/microsoftstore/).

Thanks to all the people that help out with submissions & updates & fixes!

If you encounter issues on Windows and are a developer that wants to help out, all KDE projects really appreciate patches for Windows related issues.

Just contact the developer team of the corresponding application and help us to make the experience better on any operating system.
