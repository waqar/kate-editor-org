---
title: Kate/KDevelop 2015
author: Dominik Haumann

date: 2015-10-10T20:48:57+00:00
url: /2015/10/10/katekdevelop-2015/
categories:
  - Events
  - KDE
tags:
  - planet
  - sprint

---
From the 7th to the 11th of October Kate and KDevelop contricutors once again met to work on both Kate and KDevelop.

The work in Kate was mostly spent on fixing bugs, as can be seen by the following bug chart for Kate:

[<img class="aligncenter size-full wp-image-3639" src="/wp-content/uploads/2015/10/bugcharts.png" alt="Kate Bug Charts" width="787" height="587" srcset="/wp-content/uploads/2015/10/bugcharts.png 787w, /wp-content/uploads/2015/10/bugcharts-300x224.png 300w" sizes="(max-width: 787px) 100vw, 787px" />][1]

That is, more than 300 bug and wish reports were closed. Granted, many of the reports were just old and given our limited manpower we were closing many wishes since it is unlikely that old wishes get implemented. Then again, we also fixed a lot of bugs that required code changes, and also fine tuning of KTextEditor and Kate. The fixed bugs are roughly <a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&chfield=resolution&chfieldfrom=2015-10-04&chfieldto=2015-10-11&chfieldvalue=FIXED&list_id=1301793&product=kate&product=frameworks-ktexteditor&query_format=advanced&resolution=FIXED" target="_blank">KDE bug tracker</a> (yes, the auto-brackets option is back!), besides there are also a lot of changes there were not listed in bug reports. We hope the changes are useful to you, so be sure to get the KDE Frameworks version 5.16 as soon as it&#8217;s released :-)

 [1]: /wp-content/uploads/2015/10/bugcharts.png