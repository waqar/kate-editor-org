---
title: Kate - More languages supported via LSP!
author: Christoph Cullmann
date: 2019-08-10T18:41:00+02:00
url: /post/2019/2019-08-10-kate-lsp-more-languages-supported/
---

The default configuration for the Kate LSP client does now support more stuff than just C/C++ and Python out of the box.

In addition to the recently added Rust support we now support Go and LaTeX/BibTeX, too.

### Configuration ###

The default supported server are configured via some JSON settings file we embed in our plugin resources.

Currently this looks like:

{{< highlight json >}}
{
    "servers": {
        "bibtex": {
            "use": "latex"
        },
        "c": {
            "command": ["clangd", "-log=error", "--background-index"],
            "commandDebug": ["clangd", "-log=verbose", "--background-index"],
            "url": "https://clang.llvm.org/extra/clangd/"
        },
        "cpp": {
            "use": "c"
        },
        "latex": {
            "command": ["texlab"],
            "url": "https://texlab.netlify.com/"
        },
        "go": {
            "command": ["go-langserver"],
            "commandDebug": ["go-langserver", "-trace"],
            "url": "https://github.com/sourcegraph/go-langserver"
        },
        "python": {
            "command": ["python3", "-m", "pyls", "--check-parent-process"],
            "url": "https://github.com/palantir/python-language-server"
        },
        "rust": {
            "command": ["rls"],
            "rootIndicationFileNames": ["Cargo.lock", "Cargo.toml"],
            "url": "https://github.com/rust-lang/rls"
        }
    }
}
{{< / highlight >}}

The file is located at [kate.git/addons/lspclient/settings.json](https://invent.kde.org/utilities/kate/blob/master/addons/lspclient/settings.json). [Merge requests](https://invent.kde.org/utilities/kate/merge_requests) to add additional languages are welcome.

I assume we need still to improve what we allow to specify in the configuration.

### Currently supported configuration keys ###

At the moment, the following keys inside the per-language object are supported:

#### use ####

Tell the LSP client to use the LSP server for the given language for this one, too.
Useful to dispatch stuff to a server supporting multiple languages, like clangd for C and C++.

#### command ####

Command line to start the LSP server.

#### commandDebug ####

Command line to start the LSP server in debug mode.
This is used by Kate if the LSPCLIENT_DEBUG environment var is set to 1.
If this variable is set, the LSP client itself will output debug information on stdout/stderr and the commandDebug command line should try to trigger the same for the LSP server, like e.g. using -log=verbose for clangd.

#### rootIndicationFileNames ####

For the Rust rls LSP server we added the possibility to specify a list of file names that will indicate which folder is the root for the language server. Our client will search upwards for the given file names based on the file path of the document you edit. For Rust that means we first try to locate some *Cargo.lock*, if that failed, we do the same for *Cargo.toml*.

#### url ####

URL of the home page of the LSP server implementation.
At the moment not used internally, later should be shown in the UI to give people hints where to find further documentation for the matching LSP server (and how to install it).

### Current State ###

For C/C++ with [clangd](https://clang.llvm.org/extra/clangd/) the experience is already good enough for day-to-day working.
What is possible can be seen [in one of my previous posts, video included](/posts/kate-lsp-status-july-22/).
I and some colleagues use the master version of Kate at work for daily coding.
Sometimes Kate confuses clangd during saving of files but otherwise, no larger hiccups occur.

For Rust with [rls](https://github.com/rust-lang/rls) many things work, too.
We now discover the root directory for it more easily thanks to hints to look for the Cargo files.
We adapted the client to support the *Hover* message type rls emits, too.

For the other languages: Beside some initial experiments that the servers start and you get some completion/...,
not much work went into that. Help is welcome to improve their configuration and our client code to get a better experience.

Just give Kate from the master branch a test drive, here is our [build it how-to](/build-it/).
We are open for feedback on [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org) or directly via patches on [invent.kde.org](https://invent.kde.org/utilities/kate/merge_requests).

Btw., if you think our how-to or other stuff on this website is lacking, [patches](https://invent.kde.org/websites/kate-editor-org) are welcome for that, too!
The complete page is available via our GitHub instance, to try changes locally, see our [README.md](https://invent.kde.org/websites/kate-editor-org/blob/master/README.md).
