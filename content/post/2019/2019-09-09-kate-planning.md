---
title: Kate Planning
author: Christoph Cullmann
date: 2019-09-09T17:44:00+02:00
url: /post/2019/2019-09-09-kate-planning/
---

### The Opportunity

During Akademy 2019 here in Milan, Dominik and me had time to sit together and discuss a bit what we shall do to evolve Kate :)

Whereas Kate already works well as a general purpose editor, the competition in the text editor space got more intense in the last years.

Beside the well established stuff like [GNU Emacs](https://emacs.org) & [Vim](https://www.vim.org), new editor projects got started that did set the bar higher on what users expect from a advanced GUI text editor.

For example [Sublime](https://www.sublimetext.com/), [Atom](https://atom.io) and [Visual Studio Code](https://code.visualstudio.com/) are things to keep an eye on feature & polishing wise.

Therefore we decided it would make sense to think a bit about which stuff should be improved or altered in the near future.

### The Rough Ideas

#### Kate Plugin Interfaces

From the KDE 4.x Kate to the KF5 based Kate version, we removed the Kate specific plugin interfaces and went with more generic interfaces in KTextEditor.

The idea was to be able to share more plugins e.g. between Kate and KDevelop.
This idea never really did take off, at the moment just two of our plugins are shared at all...

On the other side, this makes it much harder to expose new functionality to our plugins, as we need to stay binary compatible.

Moving back to having some Kate only stuff that has not even installed headers but is just usable for the plugins we bundle in our kate.git will make it again more flexible what to expose.

One can still think about moving "proven in practice" parts back to the KTextEditor interface part.

#### Make Projects a Core-Feature

At the moment the projects feature is located in a plugin.

For exposure of some things like the current project files some evil Qt property hacks are used.

By moving this feature into the Kate core we can use a sane way to search in project, list project stuff in quick open, ...

Other state of the art editors provide that per default, too

Still stuff like "shall we create projects on the fly" can be deactivated like today

#### LSP Support per default

We shall get the LSP support in a shape that it can be shipped enabled per default.

Users expect that modern editors provide advanced language integration off the shelf, like Atom, Visual Studio Code, ...

#### Great code navigation

We shall provide the user with better code navigation.

We will provide the plugins with an interface to register location jumps.

This will enable the user to seamlessly jump back and forth for any kind of location change e.g. due declaration lookup or search in files match lookup, ...

#### Consolidate the plugins

The new LSP plugin shall subsume stuff like the Rust or D specific code completion plugins.

Think about the future of unmaintained plugins!

#### External Tools Plugin

Revive the external tools in a improved way, e.g. like in Qt Creator.

#### Improve Port to KSyntaxHighlighting

Porting the highlighting over to KSyntaxHighlighting was a big success.

Kate and Qt Creator now finally share the same highlighting engine and we got a lot of new contributions to our highlightings.
(we support now over 300 different languages, something to be proud of)

Still, e.g. the theme support is still not moved over to what the new framework provides.

#### Further Brainstorming

##### Git Integration

We shall take a look what other editors provide.

##### Diff/Patch Viewer

Want we to integrate with stuff like KDiff3/Kompare/... to have a better handling of e.g. git diff, patches, ...?

##### Improve View Management

Try to improve the way we handle splitting the views and the tabbing.

Take a look how other editors do that!

### KDevelop vs. Kate?

Given already today we enter the area of KDevelop by providing the LSP client, we need to think about what happens in the future with overlapping features.

It is no goal to evolve Kate into an IDE.

We think Kate shall be a competitor for editors like Atom, not for full-fledged IDEs like KDevelop or Visual Studio.

Still, e.g. in the area of project management/code navigation/version control support there will be some overlap.

The question is: can we share stuff there? What shall be the focus of Kate and KDevelop in e.g. language support?

I think here it will be interesting which future direction the KDevelop project will take.

### Discussion

If you want to chime in on this, feel free to join the discussion at the [KDE reddit](https://www.reddit.com/r/kde/comments/d1sr47/kate_planning_what_is_up_for_kates_future/).

If you want to contribute, just join us at [invent.kde.org](https://invent.kde.org/utilities/kate) or on [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org).
