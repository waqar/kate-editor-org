---
title: Windows Store Status
author: Christoph Cullmann
date: 2019-12-26T20:20:00+02:00
url: /post/2019/2019-12-26-windows-store-status/
---

Kate is now in the [Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW) since September, see our [initial post](/post/2019/2019-09-12-kate-in-the-windows-store/).

It was the second application published there with the [KDE e.V.](https://ev.kde.org/) account.

One might argue that it is no good thing for an open-source project to promote the use of closed-source operating systems like Windows.

On the other side, a lot of people are stuck on Windows and I think it is a good thing to provide them with open-source software.
If people start to use more and more open-source user-space software, they will perhaps be able to switch over to some fully open-source operating system in the future.

Now the end of year is nigh and I think it is time to summarize our successes in the store in number of acquisitions (roughly equal to the number of installations, not mere downloads):

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) > 17,100 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) > 11,400 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) > 1,100 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) > 800 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) > 600 acquisitions

I think overall this doesn't look that bad.
We are no champion on Windows like [Krita](https://krita.org/), but having more than 15,000 people actually installing Kate in just one quarter of the year isn't that shabby.

If you want to help to bring more stuff KDE develops on Windows, we have some meta [Phabricator task](https://phabricator.kde.org/T9575) were you can show up and tell for which parts you want to do work on.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

Last but not least: thanks to all people that helped out with this work and I hope more will join in the future!

And: A happy new year to everybody!

Let's see what 2020 has in stock for Kate, KDE and the world.
