---
title: Kate on Windows
author: Kåre Särs

date: 2016-01-28T18:41:39+00:00
url: /2016/01/28/kate-on-windows/
categories:
  - Developers
  - KDE
  - Users

---
### **<span style="color: #ff0000;">NOTE:</span> Please refer to [Get It][1] for up-to-date Windows versions**

It took some time but here is now an installer for Kate on Windows.

First the proof :)

<figure id="attachment_3714" aria-describedby="caption-attachment-3714" style="width: 503px" class="wp-caption alignnone">[<img class="wp-image-3714 size-full" src="/wp-content/uploads/2016/01/kate-installer.jpg" alt="kate-installer" width="503" height="389" srcset="/wp-content/uploads/2016/01/kate-installer.jpg 503w, /wp-content/uploads/2016/01/kate-installer-300x232.jpg 300w" sizes="(max-width: 503px) 100vw, 503px" />][2]<figcaption id="caption-attachment-3714" class="wp-caption-text">The installer</figcaption></figure>

<figure id="attachment_3715" aria-describedby="caption-attachment-3715" style="width: 1128px" class="wp-caption alignnone">[<img class="size-full wp-image-3715" src="/wp-content/uploads/2016/01/kate-on-windows3.jpg" alt="An example of what it can look like" width="1128" height="709" srcset="/wp-content/uploads/2016/01/kate-on-windows3.jpg 1128w, /wp-content/uploads/2016/01/kate-on-windows3-300x189.jpg 300w, /wp-content/uploads/2016/01/kate-on-windows3-1024x644.jpg 1024w" sizes="(max-width: 1128px) 100vw, 1128px" />][3]<figcaption id="caption-attachment-3715" class="wp-caption-text">An example of what it can look like</figcaption></figure>

There are still some things that needs fixing. The current installer is built from git master and not from released packages so the translation stuff hat you usually get with the release packages are missing. So only partially translated. Another feature that I&#8217;m still missing is the spell-check. I need to still add a/hspell and language dictionaries to get that to work.

But for now you can try it out here: <http://download.kde.org/unstable/kate/> (updated link)

And if you are eager to help here is my scratch repo for the build recipes:  
git://anongit.kde.org/scratch/sars/kate-windows.git

And if you happen to want to have only one kate window for all the documents that you open, you might want to install DBus. This is the installer that I have tested with:  
<https://dbus-windows-installer.googlecode.com/files/DBus-Windows-Installer-1.4.1-2.exe>  
Happy Hacking!

Note: This would not have been possible without the big effort that the KDE on Windows people have made to get stuff to compile on Windows and keeping sure that it continues to compile. Thank you!

 [1]: /get-it/
 [2]: /wp-content/uploads/2016/01/kate-installer.jpg
 [3]: /wp-content/uploads/2016/01/kate-on-windows3.jpg