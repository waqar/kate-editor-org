---
title: 'KDE Software for Windows & Chocolatey'
author: Christoph Cullmann

date: 2016-06-16T19:33:59+00:00
url: /2016/06/16/kde-software-for-windows-chocolatey/
categories:
  - Common
  - Users
tags:
  - planet

---
<img class="alignright wp-image-3890" src="/wp-content/uploads/2016/06/eY1Bty7l-150x150.jpg" alt="eY1Bty7l" width="128" height="128" srcset="/wp-content/uploads/2016/06/eY1Bty7l-150x150.jpg 150w, /wp-content/uploads/2016/06/eY1Bty7l-300x300.jpg 300w, /wp-content/uploads/2016/06/eY1Bty7l.jpg 512w" sizes="(max-width: 128px) 100vw, 128px" />KDE applications providing Windows installers can now be additionally listed in the [Chocolatey][1] software repository on an dedicated [KDE profile][2]. If you want to be added to this profile with your application, contact <kde-windows@kde.org>.

At the moment, you can find digiKam, Kate & Krita. The profile allows an quick overview about the KDE applications that are available.

This is a nice addition to the downloads provided via download.kde.org or the application homepages and increases the visibility of our applications on Windows.

More info about KDE software on Windows and how to join the KDE Windows initiative can be found on our [windows.kde.org][3] page.

Would be nice if you support us to make things like this at the Randa sprint possible! ;)  
[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][4]

 [1]: https://chocolatey.org/about
 [2]: https://chocolatey.org/profiles/KDE
 [3]: https://community.kde.org/Windows
 [4]: https://www.kde.org/fundraisers/randameetings2016/