---
title: The Kate Text Editor in 2020
author: Christoph Cullmann
date: 2020-12-31T14:45:00+02:00
url: /post/2020/2020-12-31-kate-in-2020/
---

2020 was for sure no good year for most people around the world.
A lot of us are affected directly or indirectly by the currently still raging [COVID-19 pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic).
Let's hope that 2021 will bring better fortune at least in this aspect.

Still, good stuff happened in 2020, too, not all was bleak.
I read already some wrap ups of 2020 from others like [Nate](https://pointieststick.com/2020/12/31/highlights-from-2020/) and [Aleix](https://www.proli.net/2020/12/30/my-2020-with-kwin-wayland/).
A lot stuff happened inside the KDE community this year.

Kate and it's closely related parts KTextEditor & KSyntaxHighlighting evolved a lot in 2020, too.
Let's take a look at the highlights out of my perspective ;=)

### Kate got a new icon!

We refreshed our branding by introducing a [new default icon](https://kate-editor.org/post/2020/2020-01-30-new-breeze-kate-icon/) for Kate designed by Tyson Tan.

<p align="center">
    <a href="/images/kate.svg" target="_blank"><img width=512 src="/images/kate.svg"></a>
</p>

Even thought this is no functional change, for a first time in the now 20 years lifetime of Kate we have some instantly recognizable icon.
Yes, it doesn't show it is an text editor, but the icons of Atom/Sublime/VS Code and Co. don't do that neither.
But you can finally remember it after seeing it a few times => the bird is Kate ;=)

Thanks again to Tyson Tan for spending his time to work on this.
And thanks to Noah Davis & Nate Graham to get this into Breeze, too.
Even "just" changing the icon is a real team effort!

### Our LSP client matured

Already in 2019 with 19.12 we had the first release of our LSP client plugin.
In 2020 this plugin matured even further since the [latest status report](https://kate-editor.org/post/2020/2020-01-01-kate-lsp-client-status/).

<p align="center">
    <a href="/post/2020/2020-12-31-kate-in-2020/images/kate-lsp.png" target="_blank"><img width=700 src="/post/2020/2020-12-31-kate-in-2020/images/kate-lsp.png"/></a>
</p>

Mark Nauwelaerts did a brilliant job in creating and further polishing this feature!

I use this plugin now daily at work and am very happy with the current state.

If you still have some itch to scratch there, please contribute.
The plugin is well structured and it is easy to extended, for example Waqar Ahmed just provided support for ["Find Implementations"](https://invent.kde.org/utilities/kate/-/merge_requests/152).

### Tabs, tabs, tabs...

One of the largest visible UI changes in 2020 in the Kate application itself was the return of the classical tabbing in 20.08 and then again the addition of optional LRU like tabbing in [20.12](https://kate-editor.org/post/2020/2020-09-15-kate-and-the-tab-bar-release-20.12/).

<p align="center">
    <a href="/post/2020/2020-12-31-kate-in-2020/images/kate-tabs.png" target="_blank"><img width=700 src="/post/2020/2020-12-31-kate-in-2020/images/kate-tabs.png"/></a>
</p>

As can be seen above, since then Kate learned in the master branch a bit more fine grained configuration of the tabbing.
You want LRU like tabbing => set a limit for number of tabs.
You like larger expanding tabs?
You dislike the double/middle-click behavior?
=> Configure it like you want it to be!

### KTextEditor & KSyntaxHighlighting Color Themes

After years of procrastination KTextEditor finally started to use the color themes as provided by KSyntaxHighlighting.
Details of this change can be found in the two summaries [here](https://kate-editor.org/post/2020/2020-09-06-kate-color-themes/) and [there](https://kate-editor.org/post/2020/2020-09-13-kate-color-themes-5.75/).
Unfortunately the configuration dialog in KTextEditor had still some faults in 5.75, these should be rectified in 5.76 and higher.

As this was an incompatible change, especially regarding user defined themes, this wasn't done lighthearted but it was required to have some future proof theme support.
The new theme storage is a standalone JSON file.
The same format is used for the shipped themes as for the user defined ones.
No longer some special storage inside some misused KConfig files.

If you have a lot of local old theme config, the KSyntaxHighlighting repository contains some python based converter scripts.
The are inside the [utils folder](https://invent.kde.org/frameworks/syntax-highlighting/-/tree/master/utils/).

Kate provides now already [19 default themes](/themes/).
And as this is a KSyntaxHighlighting & KTextEditor feature, all other applications using this like KDevelop benefit, too.
You can now have out of the box nice themes like **ayu Mirage**:

{{% include "/static/themes/html/ayu-mirage-snippet.html" %}}

If you want to provide more themes, please [submit some merge request](https://kate-editor.org/post/2020/2020-09-18-submit-a-ksyntaxhighlighting-color-theme/).

### Kate on Windows

Kate is now more than one year in the official [Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW).
And it seems we are not that unloved there, see [the latest in detail status report](https://kate-editor.org/post/2020/2020-09-28-kate-windows-store-current-status/).

Our rating even improved since that report, see below today's state!

<p align="center">
    <a href="/post/2020/2020-12-31-kate-in-2020/images/kate-windows-reviews.png" target="_blank"><img width=500 src="/post/2020/2020-12-31-kate-in-2020/images/kate-windows-reviews.png"/></a>
</p>

Today, 31.12.2020, 75,889 Kate installations were done via the store alone since we launched there last year!

Thanks to all that helped to make this happen, like Hannah!

### Short interlude: Kate turned twenty!

Finally, in December, [Kate turned twenty](/post/2020/2020-12-14-kate-is-20-years-old/). How it all began:

<p align="center">
    <a href="/wp-content/uploads/2011/08/kate-in-kde2.2.png" target="_blank"><img width=700 src="/wp-content/uploads/2011/08/kate-in-kde2.2.png"></a>
</p>

Yes, we used stuff without anti-aliasing in the ancient past.

### Most important: Our Contributors!

All above mentioned things are cool, but what keeps Kate and it's KF5 frameworks base afloat are the people that contribute.

Perhaps this is just a feeling, as I didn't do proper statistics about it, but I believe since we switched over the development to our GitLab instance at [invent.kde.org](https://invent.kde.org), more contributions are arriving.

[Contributing via GitLab merge requests](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) seems to really be more easy especially for new people.
We already merge:

* 129 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 48 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

Naturally a lot of these patches are small things, but even they matter!

You found some wrong documentation => sure we are interested in a fix!

You did create color theme that might be worth sharing => hand it in!

You wrote a JSON configuration for a LSP server we forgot => we want that!

Our automatically generated [team page](/the-team/) now already lists over 560 contributors.
Join our ranks and help us to make our stuff awesome!

Thanks to everybody that helped to improve our stuff!

### New Year Wishes

Let's hope 2021 will be a better year for the world!

And I hope we will see a further rise in contributors & contributions for Kate and KDE in general.

Let's rock!
