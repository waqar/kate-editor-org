---
title: KSyntaxHighlighting - Example Files Wanted
author: Christoph Cullmann
date: 2020-08-23T17:09:00+02:00
url: /post/2020/2020-08-23-ksyntaxhighlighting-example-files-wanted/
---

In the [KSyntaxHighlighting framework](https://invent.kde.org/frameworks/syntax-highlighting) we use small example files for the individual languages as regression tests.
See the current collection [here](https://invent.kde.org/frameworks/syntax-highlighting/-/tree/master/autotests/input).

We will create HTML output and two internal formats to check highlighting attributes & folding regions.
These results are then diffed with version controlled reference files.

At the moment we only check the default light theme for the HTML output, but I intend to extend this to check the dark theme, too.
This will e.g. make it easier to spot problematic hard coded colors that are not readable in one of both variants.

I updated our tooling to link the test output HTML files we actually have already on our [syntax overview page](/syntax/).

As you can see there, still a lot of languages we support are lacking example files.
Just scan the page for the "submissions welcome" lines ;)
These words are linked to the part of [our README] (https://invent.kde.org/frameworks/syntax-highlighting#adding-unit-tests-for-a-syntax-definition) that talks about our regression tests.
If you can provide an example file under a permissive license (MIT/BSD/GPL/...), please submit them e.g. as [merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests).

Naturally it would be nice to cover many different syntax constructs in the file, but even a small file only covering parts is a lot better than no coverage at all!

If a merge request is too much work and given we just speak about single file contributions, you can mail the file with a stated license to use (and if taken from somewhere please a reference from where)
to [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org), I will in-cooperate them then.
You don't even need to subscribe to the list, I can moderate the incoming mail.

Having more test files will allow us to avoid regressions during optimizations & improvements to the highlighting engine.
And it will make it easier to alter the existing language files, too.

Naturally, if you detect some defects during your example creation, patches to the syntax definitions are welcome [here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests), too!

And even cooler: You can provide a completely new highlighting together with an example, just go to [the same place](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests) and submit it!
