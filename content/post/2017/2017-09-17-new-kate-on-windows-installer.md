---
title: New Kate on Windows installer
author: Kåre Särs

date: 2017-09-17T20:28:57+00:00
url: /2017/09/17/new-kate-on-windows-installer/
categories:
  - Common

---
### **<span style="color: #ff0000;">NOTE:</span> Please refer to [Get It](/get-it/) for up-to-date Windows versions**

We have a new round of Kate installers for Windows. This round comes with some installer-bug fixes (all project plugin dlls) and support for editorconfig files http://editorconfig.org/.

[<img class="aligncenter size-full wp-image-4067" src="/wp-content/uploads/2017/09/Screenshot_20170917_220757.png" alt="Project plugin analyzing code" width="684" height="478" srcset="/wp-content/uploads/2017/09/Screenshot_20170917_220757.png 684w, /wp-content/uploads/2017/09/Screenshot_20170917_220757-300x210.png 300w" sizes="(max-width: 684px) 100vw, 684px" />][1]In the above screen-shot you can see the project plugin analyzing Kate code. This works if you have cppcheck installed and added to the path.

Grab the installers now at download.kde.org:  [Kate-setup-17.08.1-KF5.38-32bit][2] or [Kate-setup-17.08.1-KF5.38-64bit][3]

 [1]: /wp-content/uploads/2017/09/Screenshot_20170917_220757.png
 [2]: https://download.kde.org/stable/kate/Kate-setup-17.08.1-KF5.38-32bit.exe
 [3]: https://download.kde.org/stable/kate/Kate-setup-17.08.1-KF5.38-64bit.exe
