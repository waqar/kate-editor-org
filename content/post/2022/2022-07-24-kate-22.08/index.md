---
title: Kate 22.08
author: Christoph Cullmann
date: 2022-07-24T20:12:00+02:00
url: /post/2022/2022-07-24-kate-22.08/
---

## KDE Gear 22.08 Release in August

Next month the [KDE Gear 22.08](https://community.kde.org/Schedules/KDE_Gear_22.08_Schedule) release will arrive.
Kate and KWrite are released with that collection of KDE software.

Thanks already to all people that make KDE Gear releases possible.
I myself would not be able to release twice a year some proper version with patch releases, manage the i18n stuff, ....
It is really a lot of work to get this done and I am very grateful to the KDE team members that take care of that!
If you maintain some invent.kde.org hosted KDE application and want to have regular releases, I strongly suggest to join KDE Gear.

## Current Release Branch Status

The release branch is already created and we will only backport bug fixes to 22.08, the master branch is open for normal development.
As one can see on in the [commit history](https://invent.kde.org/utilities/kate/-/commits/release/22.08/) of the 22.08 branch, Waqar and others helped to get some last minute fixes in.

## Kate in 22.08

22.08 will be no revolutionary jump for Kate, more a nice incremental update containing a lot of small improvements and bug fixes compared to 22.04.

If you want to dive in more deep, we have a nice list of all [merge requests](/merge-requests/) that give you a proper overview about the larger development tasks that did take place.

Naturally, there are some user visible top level changes to Kate.
Per default, we now have some text free left & right border with just a bit larger icons.
One can naturally switch back to the old style and adjust the icon size there.
The bottom status/tool button/... border got fine tuned, too.

![Screenshot showing Kate 22.08](/post/2022/2022-07-24-kate-22.08/images/kate-22.08.webp)

Hopefully now the default look'n'feel will be a lot more slick then in former releases.

Under the hood more stuff changed naturally.
We worked to improve the LSP plugin, the project handling, Git integration, [KTextEditor has now multi-cursor support](/post/2022/2022-03-10-ktexteditor-multicursor/) and more.

## KWrite in 22.08

As mentioned in my [previous post](/post/2022/2022-03-31-kate-ate-kwrite/) KWrite now is just a slimmed down Kate.
This means KWrite is just a 200 lines main.cpp file that uses the normal Kate code base and just shrinks it down to a non-plugin supporting slimmer variant via some internal options.

![Screenshot showing KWrite 22.08 with tabs](/post/2022/2022-07-24-kate-22.08/images/kwrite-22.08.png)

As the screenshot shows, this means KWrite has now proper support for tabs (and split views).
If supports now quick open, the HUD for commands and many more nifty Kate features.
As this is the first release that will use the unified code base, for sure there will be some quirks that are yet to be uncovered.
But this should be some nice foundation to provide both applications in the future without a lot of extra work.

Btw., this means KWrite will now support opt-in telemetry information, just like Kate.
For details see the [KDE Telemetry Use](https://community.kde.org/Telemetry_Use) page.

# Help wanted!

I hope you will enjoy the 22.08 release, but we need more people to improve our applications even further.

If you want to help us out, show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

We are always searching for more help to make both our editors (and the underlying frameworks) even more outstanding!

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/w723b8/the_kate_editor_upcoming_release_2208/).
