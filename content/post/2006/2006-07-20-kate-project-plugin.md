---
title: Kate Project Plugin
author: Dominik Haumann

date: 2006-07-20T08:24:00+00:00
url: /2006/07/20/kate-project-plugin/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/07/kate-project-plugin.html
categories:
  - Common

---
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://photos1.blogger.com/blogger/4106/1694/1600/kateprojectmanager.png"><img style="margin: 0pt 0pt 10px 10px; float: right; cursor: pointer;" src="http://photos1.blogger.com/blogger/4106/1694/320/kateprojectmanager.png" alt="" border="0" /></a>It is not a secret that removing Kate&#8217;s project manager in KDE 3.5 was not the right thing to do. It seems a lot of people used it and we got [many complaints][1] about this decision. That also shows that it is hard go get feedback about what users are really using. If a feature is done well, noone will ever talk about it. This is paradox, as we thought the project manager was not well integrated :) (I still think that)

Ok, let&#8217;s come to the interesting part: Three developers wrote a new [<span style="font-style: italic;">Kate Project Manager Plugin</span>][2] and published an initial release on sourceforge.net and on [kde-apps.org][3]. It works quite well already and reading the comments on kde-apps.org gives me the impression that there are more features to come with the next release. &#8212; If you are interested maybe you want to join the project&#8230;

PS: I have the secret hope the plugin will be ported to KDE4 using Qt&#8217;s model/view architecture, so that it has good support for multiple mainwindows :)

 [1]: http://bugs.kde.org/show_bug.cgi?id=116946
 [2]: http://sourceforge.net/projects/kate-prj-mng/
 [3]: http://www.kde-apps.org/content/show.php?content=42653