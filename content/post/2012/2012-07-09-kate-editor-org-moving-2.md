---
title: kate-editor.org moving
author: Christoph Cullmann

date: 2012-07-09T21:18:56+00:00
url: /2012/07/09/kate-editor-org-moving-2/
pw_single_layout:
  - "1"
categories:
  - Common

---
The Kate homepage will move to a new faster server.

There will be some downtime in the next days, as DNS update will take some time and stuff needs to be setup on the new machine.

Hope we are back with an even faster homepage afterwards ;)