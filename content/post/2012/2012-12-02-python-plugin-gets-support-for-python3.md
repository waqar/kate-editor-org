---
title: Python plugin gets support for Python3
author: shaheed

date: 2012-12-02T18:29:39+00:00
url: /2012/12/02/python-plugin-gets-support-for-python3/
pw_single_layout:
  - "1"
categories:
  - Common
tags:
  - python

---
Being vaguely aware that Python3 had some &#8220;interesting&#8221; differences compared to Python2, I had decided to not think about Python3 for now, but then one of our dear users [piped up][1] to say that even building it was broken! That seemed weird, so I started poking around only to find myself falling Alice-like into a Wonderland where strings were not always strings&#8230;

Well, I&#8217;ve long been interested in i18n and l10n in all their forms, especially as they apply to Indic languages, so I was somewhat aware of the sorts of issues that Unicode can throw up. Luckily, as a KDE developer I&#8217;m used to depending on QString handle all the routine grunt work so it was a bit of a rude awakening to discover that, the C API for Python strings takes many forms:

  * Python 2.x has 2 run-time variants with 3 compile-time variants. 
      * <a href="http://docs.python.org/2/c-api/unicode.html" target="_blank">http://docs.python.org/2/c-api/unicode.html</a>
  * Python 3.2 or less has 3 compile-time variants 
      * <a href="http://docs.python.org/3.2/c-api/unicode.html#unicode-objects" target="_blank">http://docs.python.org/3.2/c-api/unicode.html#unicode-objects</a>
  * Python 3.3 or greater has 3 run-time variants 
      * <a href="http://docs.python.org/3.3/c-api/unicode.html#unicode-objects" target="_blank">http://docs.python.org/3.3/c-api/unicode.html#unicode-objects</a>

<div>
  Note only that, but:
</div>

<div>
  <ul>
    <li>
      Ubuntu does not yet have 3.3.
    </li>
    <li>
      The cmake support in KDE before 4.9.4 cannot find the right libraries.
    </li>
    <li>
      The PyKDE4 support for strings was broken-then-fixed.
    </li>
    <li>
      Python3 pickles structures differently than Python2.
    </li>
  </ul>
</div>

<div>
  Anyway, with some excellent support from Luca Beltrame and Alex Turbov, I&#8217;m glad to say that Pate and its plugins should now work with any of Python 2.x, <= 3.2, or 3.3 in the upcoming 4.9.4 and 4.10 releases. Thanks Luca and Alex!
</div>

<div>
</div>

 [1]: https://bugs.kde.org/show_bug.cgi?id=310220 "piped up"