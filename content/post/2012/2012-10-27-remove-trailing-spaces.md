---
title: Remove Trailing Spaces
author: Dominik Haumann

date: 2012-10-27T14:01:05+00:00
url: /2012/10/27/remove-trailing-spaces/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Up to KDE 4.9, Kate Part had support to remove trailing spaces in two ways:

  1. Remove trailing spaces while editing
  2. Remove trailing spaces on save

The reasoning behind removing trailing spaces while editing is that when working on a document, we want to keep our own changes clean of trailing spaces. This way, we can for instance provide patches that are not cluttered with whitespace changes, and we just change lines that we _really_ want to change.

The implementation of this feature unfortunately had quite some regressions that we were able to &#8220;fix&#8221; over time. For instance, you do not want to remove trailing spaces if the cursor is currently in the trailing spaces area. This alone means we have to kind of remember that we touched this line, and then remove it later. This was always hacky, and in fact, there are still corner cases that did not work.

For KDE 4.10, the both options were merged into just one option **Remove trailing spaces** with three possible values:<img class="aligncenter size-full wp-image-2060" title="Remove Trailing Spaces in KDE 4.10" src="/wp-content/uploads/2012/10/remove-trailing-spaces.png" alt="" width="542" height="435" srcset="/wp-content/uploads/2012/10/remove-trailing-spaces.png 542w, /wp-content/uploads/2012/10/remove-trailing-spaces-300x240.png 300w" sizes="(max-width: 542px) 100vw, 542px" />

So we only support removing trailing spaces on save from KDE 4.10 on. The implementation is now very clean and based on the [line modification system][1] available since KDE 4.8: Thanks to this system we know exactly which lines in the document were changed. So if you choose &#8220;Modified Lines&#8221; in the configuration, trailing spaces of these modified lines are removed, and other lines remain untouched. If you choose &#8220;Entire Document&#8221;, then all trailing spaces in the document will be removed. And, needless to say, &#8220;Never&#8221; implies that trailing spaces are never removed.

For compatibility, the old mode-lines &#8220;remove-trailing-space&#8221; and &#8220;replace-trailing-spaces-save&#8221; are still supported, but you&#8217;ll get a kWarning() on the console. All these changes are also [documented in the Kate handbook][2] (once KDE 4.10 is released). From KDE 4.10 on, you should switch to the modelines

> &#8211; remove-trailing-spaces none;  
> &#8211; remove-trailing-spaces modified;  
> &#8211; remove-trailing-spaces all;

Hope you like it&#8230;

 [1]: /2011/09/06/line-modification-system/ "Line Modification System"
 [2]: http://docs.kde.org/stable/en/kde-baseapps/kate/config-variables.html#config-variables-list "Kate Document Variables"