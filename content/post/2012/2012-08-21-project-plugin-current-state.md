---
title: Project Plugin, Current State
author: Christoph Cullmann

date: 2012-08-20T22:30:18+00:00
url: /2012/08/21/project-plugin-current-state/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After some days of more hacking on the plugin, there current state is already nice for my daily use.

A simple file like

<pre>{
     "name": "Kate"
   , "files": [ { "git": 1 } ]
}</pre>

defines already the complete project for the kate.git.

If you open any file with Kate inside your local kate.git clone (and the project plugin from master branch is loaded), Kate will auto-open the project and highlight the file you just opened there in the tree. No need to think about opening some project in the menu, just open the file you want to start working at and start. The project plugin won&#8217;t get into your way or require additional steps.

It will allow you nice and fast file switching between your project files in the filesystem tree like structure without any noise of non-git tracked files ;) Still the normal &#8220;Documents&#8221; view is around like it used to be, if you want to just navigate between currently opened files.

If you modify the project file, the project will auto-refresh itself, still a reload button is needed to trigger e.g. reparsing of git files, if your add/remove files and want a fresh project tree. The auto-reload won&#8217;t help there, as is is more or less a nop if the project file content was unchanged.

The &#8220;Search and Replace&#8221; plugin will integrate nicely and per default will do project wide searches, if any project is currently active (you can switch to other search modes like before in the combobox, just the default is changed, if the project plugin is active). Thanks to Kåre&#8217;s nice design of the plugin, this addition was really easy to implement.

Exuberant Ctags integration is work-in-progress. At the moment on project load, a background thread will generate a ctags index (after it has constructed the file tree from git/svn/&#8230;) and the auto-completion will use this index for all files associated with your project. An additional toolview, that allows you to search inside the index will be implemented, too. It is a bit like the current ctags plugin, but without any setup or manual indexing ;) For the kate.git, the indexing (in the background) needs less than half second, even for kdelibs frameworks branch it is only 2-3 seconds. On Kate exit the index files (created as temp files in your local temp directory) will vanish again. No index database polluting your checkouts/clones or homes.

Will try to add more features I like for my daily work, if you have any ideas to improve the plugin, just let me know here or on bugs.kde.org ;)