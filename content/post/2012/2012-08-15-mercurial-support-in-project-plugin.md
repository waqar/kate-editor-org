---
title: Mercurial Support in Project Plugin
author: Dominik Haumann

date: 2012-08-15T13:28:13+00:00
url: /2012/08/15/mercurial-support-in-project-plugin/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Christoph already wrote [several blog posts][1] about the [upcoming projects plugin][2]. In his last blog, [he mentioned][3] that a project can now be created on-the-fly by reading the git or subversion output: All the files under version control are automatically listed as files in the project.

However, this is restricted to <a title="Project from git" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/entry/kate/plugins/project/kateproject.cpp#L216" target="_blank">git</a> and <a title="Project from svn" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/entry/kate/plugins/project/kateproject.cpp#L250" target="_blank">subversion</a>. If you always wanted to contribute to Kate, you can give other version control systems a try, such as Mercurial (command: hg manifest). Just [quickly build Kate][4], and [send us a patch][5]!

**Update (2013-10-31):** Support was added in commit on <a title="Mercurial Support in Kate Projects" href="http://quickgit.kde.org/?p=kate.git&a=commit&h=8531af4d7a3c5a1a64a09dfa3796c4ffbd4f54fa" target="_blank">2013-03-02</a> and is available in KDE 4.10.5 and later versions.

 [1]: /2012/08/05/project-management/ "Project Management"
 [2]: /2012/08/06/project-management-take-two/ "Kate Projects Plugin"
 [3]: /2012/08/12/project-management-take-three/ "Version Control in Projects Plugin"
 [4]: /get-it/ "Build Kate"
 [5]: mailto:kwrite-devel@kde.org "Mailinst List"