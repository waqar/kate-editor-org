---
title: Some Kate Color Schemas
author: Dominik Haumann

date: 2012-03-07T21:31:18+00:00
url: /2012/03/07/some-kate-color-schemas/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
[As already mentioned][1], Kate Part much better chooses colors from the default KDE color palette configured in System Settings in upcoming KDE SC 4.9. As teaser, here are some examples &#8211; hope you like it.

<p style="text-align: center;">
  <strong>Default &#8220;Oxygen&#8221; Color Schema</strong><br /> <a href="/wp-content/uploads/2012/03/oxygen.png"><img class="aligncenter size-full wp-image-1729" title="Oxygen Color Schema" src="/wp-content/uploads/2012/03/oxygen.png" alt="" width="739" height="410" srcset="/wp-content/uploads/2012/03/oxygen.png 739w, /wp-content/uploads/2012/03/oxygen-300x166.png 300w" sizes="(max-width: 739px) 100vw, 739px" /></a>
</p>

<p style="text-align: center;">
  <strong>Default &#8220;Obsidian Coast&#8221; Color Schema</strong> <a href="/wp-content/uploads/2012/03/obsidian-coast.png"><img class="aligncenter size-full wp-image-1730" title="Obsidian Coast Color Schema" src="/wp-content/uploads/2012/03/obsidian-coast.png" alt="" width="739" height="410" srcset="/wp-content/uploads/2012/03/obsidian-coast.png 739w, /wp-content/uploads/2012/03/obsidian-coast-300x166.png 300w" sizes="(max-width: 739px) 100vw, 739px" /></a>
</p>

<p style="text-align: center;">
  <strong>Dark &#8220;Vim&#8221; Color Schema</strong><br /> <a href="/wp-content/uploads/2012/03/vim.png"><img class="aligncenter size-full wp-image-1731" title="Vim Color Schema" src="/wp-content/uploads/2012/03/vim.png" alt="" width="739" height="410" srcset="/wp-content/uploads/2012/03/vim.png 739w, /wp-content/uploads/2012/03/vim-300x166.png 300w" sizes="(max-width: 739px) 100vw, 739px" /></a>
</p>

<p style="text-align: left;">
  Note: The yellow search background comes from the &#8220;Highlight Selection&#8221; plugin, which is still a hard-coded yellow. Kate Part&#8217;s &#8220;Search & Replace&#8221;chooses more fitting highlight colors for matching and replaced text.
</p>

 [1]: /2012/03/01/color-settings/ "Color Settings"